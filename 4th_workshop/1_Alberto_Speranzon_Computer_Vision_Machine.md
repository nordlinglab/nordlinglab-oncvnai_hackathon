# [4th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-04/)
## Speaker: Dr. Alberto Speranzon, Technical Fellow at Honeywell Aerospace
## Title: [Computer Vision and Machine Learning for the Aerial Autonomy](https://www.nordlinglab.org/workshop-oncvai-04/#RAS)

Takeaways: 

* Computer vision can eliminate the need for extended tasks requiring human supervision 

Sections of the talk:
### [**Topic**](https://youtu.be/SFIJBx0V1CU?t=193)
* Image processing of infrastructure using UAV
* CV for urban air mobility (parcel delivery etc..)

### [**Inpection of infrastructure with UAV**](https://youtu.be/SFIJBx0V1CU?t=241)
* AIM:
    *  have drone capture video of struture
    *  analyze footage with ML
    *  to reduce time of operator watching drone video

    #### [**Crack Detection**](https://youtu.be/SFIJBx0V1CU?t=344)
    * [Workaround to detect faint cracks](https://youtu.be/SFIJBx0V1CU?t=435)
    * [Crack Detection Results](https://youtu.be/SFIJBx0V1CU?t=511)
    
    #### [**UAV to register location of electricity power line poles**](https://youtu.be/SFIJBx0V1CU?t=640)
    * [Point cloud 3-D spatial representation](https://youtu.be/SFIJBx0V1CU?t=712)
    * [Why not use Deep learning model to discern poles from trees?](https://youtu.be/SFIJBx0V1CU?t=748)
    * [Why segmentation instead of an object classifier?](https://youtu.be/SFIJBx0V1CU?t=843)
    * [How was the segmentation of the poles done?](https://youtu.be/SFIJBx0V1CU?t=920)

### [**CV for urban air mobility UAM**](https://youtu.be/SFIJBx0V1CU?t=1042)
* increasing demand for faster shipping
* [near future UAM projects  ](https://youtu.be/SFIJBx0V1CU?t=1134)
  * [Focus on simple driving system --> Autonomy](https://youtu.be/SFIJBx0V1CU?t=1195)
* [Pipeline of autonomous system: OODA](https://youtu.be/SFIJBx0V1CU?t=1271)
    * [Observe](https://youtu.be/SFIJBx0V1CU?t=1304)
    * [Orient](https://youtu.be/SFIJBx0V1CU?t=1351)
        * [Landing Detection](https://youtu.be/SFIJBx0V1CU?t=1391)
        * [Emergency landing](https://youtu.be/SFIJBx0V1CU?t=1595)
    * [Decide & Act](https://youtu.be/SFIJBx0V1CU?t=1792)
        * System must be able to evaluate comlpex scenarios and choose safe actions

### [**Objective and Challenges**](https://youtu.be/SFIJBx0V1CU?t=1936)

## Q&A 

1. [What could be more accutrate than NN?](https://youtu.be/SFIJBx0V1CU?t=2132)

2. [Does crack detection also work on not smooth surface?](https://youtu.be/SFIJBx0V1CU?t=2240)

2. [Is the QR safe? Can a fake QR code misled the vehicles's route?](https://youtu.be/SFIJBx0V1CU?t=2326)

2. [How to convince public about automatic vehicle safety?](https://youtu.be/SFIJBx0V1CU?t=2449)

2. [Can small other small object be detected?](https://youtu.be/SFIJBx0V1CU?t=2560)

1. [How well will drones interact with other aircrafts?](https://youtu.be/SFIJBx0V1CU?t=2678)