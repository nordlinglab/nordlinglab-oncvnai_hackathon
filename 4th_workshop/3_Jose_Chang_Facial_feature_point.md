# [4th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-04/)
## Speaker: Jose R. Chang, Ph.D. student in the Nordling Lab in National Cheng Kung University
## Title: Facial feature point detection using deep feature encodings

Takeaways: 

* Feature tracking methods: SIFT, SURF, and Lucas Kanade
* New method to track skin features

Sections of the talk:
### [**Topic: Use CV to develop non-invasive heart rate quantifier**](https://youtu.be/_ugcR2hzFZI?t=295)
* [What: Feature tracking](https://youtu.be/_ugcR2hzFZI?t=428)
* [How: Convolutional stacked autoencoder](https://youtu.be/_ugcR2hzFZI?t=489)

### [**Issues with current state of the art tracking algorithms**](https://youtu.be/_ugcR2hzFZI?t=565)
* Sensitive to environmental factors
* Often reliable only in a strict set of conditions

### [**Pipeline characteristics**](https://youtu.be/_ugcR2hzFZI?t=804)
* More precise than other methods such as SIFT & SURF

### [**Face Feature Tracking in applications CV**](https://youtu.be/_ugcR2hzFZI?t=930)
* Snapchat filters
* Recognizing smiles, grins, drowsiness

### [**Feature Tracking Def**](https://youtu.be/_ugcR2hzFZI?t=1006)
* [SIFT feature tracking example](https://youtu.be/_ugcR2hzFZI?t=1038)


### [**Image Registration Techniques**](https://youtu.be/_ugcR2hzFZI?t=1104)
   * Feature Based (SIFT, SURF)
   * Photometric Intensity Based (Lucas-Kanade)
   * [SIFT](https://youtu.be/_ugcR2hzFZI?t=1180)
   * [SURF](https://youtu.be/_ugcR2hzFZI?t=1383)
   * [Lucas-Kanade](https://youtu.be/_ugcR2hzFZI?t=1576)
 
### [**History of object tracking in Deep Learning**](https://youtu.be/_ugcR2hzFZI?t=1774)
* Iteratively track target across images
* Difficulties of tracking small features in faces as opposed to bigger objects (cars, pedestrians, etc..)

### [**Objective**](https://youtu.be/_ugcR2hzFZI?t=1920)
* Create an unsupervised model, free of bias, that can work in an flexible range of environmental conditions

### [**Training Dataset: UTK Face**](https://youtu.be/_ugcR2hzFZI?t=2015)
* Broad range of faces
* Labels: age, gender, ethnicity
* [OpenCV Resnet 10: ](https://youtu.be/_ugcR2hzFZI?t=2142)
    * Get only the face from each picture
    * Discard everything else 
* [How are patches cropped for the encoder??](https://youtu.be/_ugcR2hzFZI?t=2202)

### [**Validation Dataset**](https://youtu.be/_ugcR2hzFZI?t=2285)
* Remote ballistocardiography dataset
    1. Static motion
    2. Bike motion
* Parkinson's postural tremor test

### [**Process**](https://youtu.be/_ugcR2hzFZI?t=2494)
* Analysis using deep feature enconding, as well a other methods to compare results
    * SIFT, SURf & Lucas-Kanade --> grayscale
    * Deep Feature Encoder --> CIELAB color space
* [Advantages CIELAB color space](https://youtu.be/_ugcR2hzFZI?t=2703)

* [How are the feature batches evaluated as the pass through the encoder??](https://youtu.be/_ugcR2hzFZI?t=2764)

#### [What is Subpixel level predictions?](https://youtu.be/_ugcR2hzFZI?t=2837)
  * [Intutition](https://youtu.be/_ugcR2hzFZI?t=2914)
  * [Mathemathics of the approach](https://youtu.be/_ugcR2hzFZI?t=2961)

#### [Chi squared analysis](https://youtu.be/_ugcR2hzFZI?t=3046) : used to discern model innherent errors from labeling errors 
  * [Error thresholds and significance level relationship](https://youtu.be/_ugcR2hzFZI?t=3120)

#### Deep Feature Encodings
  * [Concept behind auto encoder](https://youtu.be/_ugcR2hzFZI?t=3198)
  * [Encoder Network Architecture](https://youtu.be/_ugcR2hzFZI?t=3283)
    * Number of conv layers, kernel size
    * Conv --> batch normalization --> activation function
    * Input size = output
    *  31x31x3 --> 1x1x128 --> 31x31x3
    *  [What does Batch normalization do?](https://youtu.be/_ugcR2hzFZI?t=3414)

### [**Results**](https://youtu.be/_ugcR2hzFZI?t=3572)
* [Error in pixels comparison](https://youtu.be/_ugcR2hzFZI?t=3586)
* [Samples above threshold depending on significance level](https://youtu.be/_ugcR2hzFZI?t=3665)
* [Distance of first & second nearest neighbors](https://youtu.be/_ugcR2hzFZI?t=3763)
* [Number of nearest neighbors below 99% confidence interval](https://youtu.be/_ugcR2hzFZI?t=3874)
* [Sum of Square Residuals](https://youtu.be/_ugcR2hzFZI?t=3943)

## **Q&A**
1. [Applications?](https://youtu.be/_ugcR2hzFZI?t=4163)

1. [Improvement plans?](https://youtu.be/_ugcR2hzFZI?t=4377)