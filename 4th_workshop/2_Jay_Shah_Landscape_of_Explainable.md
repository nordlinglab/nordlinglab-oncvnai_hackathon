# [4th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-04/)
## Speaker: Mr. Jay Shah, Ph.D. student at Arizona State University
## Title: [Landscape of Explainable AI, Interpreting Deep Learning predictions and my observations from hosting an ML Podcast](https://www.nordlinglab.org/workshop-oncvai-04/#RJS)

Takeaways: 

* Many black-box models are used in sensitive fields that influence our lives, so understanting these models is important

Sections of the talk:
### [**Explainability vs Interpretability**](https://youtu.be/r8OLs_1XUvc?t=484)
* ML succesful in various fields: 
    * medical field, legal system, financial trading, banking systems
* [Interpretability: Why is it necessary?](https://youtu.be/r8OLs_1XUvc?t=586)
    * Dangers of of using **"black box"** models for sensitive applications. (healthcare, law, etc..) 
* [Difference between **Reliability** & **Interpretability**](https://youtu.be/r8OLs_1XUvc?t=811)

### **Two approaches to understand models**
* [ Interpretable --> Linear models ](https://youtu.be/r8OLs_1XUvc?t=873)
    * possible to understand how model features interact to give prediction
* [Explainable --> Non-linear models](https://youtu.be/r8OLs_1XUvc?t=929)
    * We don't know how the predicition is derived from the features; we attemp to explain results instead.
* [Tradeoff between interpretability and acuraccy](https://youtu.be/r8OLs_1XUvc?t=1011)
* [When to choose which type of model?](https://youtu.be/r8OLs_1XUvc?t=1062)

### **Explanation Methods**
* [Characteristics model *explanations* should meet?](https://youtu.be/r8OLs_1XUvc?t=1122)
* [Interpretation method categories](https://youtu.be/r8OLs_1XUvc?t=1213)
    * Model specific
    * Model agnostic: applicable to any model
* [Explanation method Categories](https://youtu.be/r8OLs_1XUvc?t=1239)
    * Local explanation: single prediction
    * Global explanations: whole model training
* [*Local* explanation methods](https://youtu.be/r8OLs_1XUvc?t=1260)
    * Feature Importance Methods
        * [Shapley approach](https://youtu.be/r8OLs_1XUvc?t=1298)
        * [Lime](https://youtu.be/r8OLs_1XUvc?t=1358)
    * [ConterFactual](https://youtu.be/r8OLs_1XUvc?t=1455)
    * [Instance Based](https://youtu.be/r8OLs_1XUvc?t=1492)
    * [Saliency Maps](https://youtu.be/r8OLs_1XUvc?t=1527)

* *Global* explanation methods
    * [Concept-based technique](https://youtu.be/r8OLs_1XUvc?t=1565)
    * [Proxy Models/Model Distillation](https://youtu.be/r8OLs_1XUvc?t=1623)
    * [Aggregating local explanations](https://youtu.be/r8OLs_1XUvc?t=1660)
  
### [**Limitations:**](https://youtu.be/r8OLs_1XUvc?t=1710)
* Reliability & Thruthfulness
  
### **Speaker's Research:**
* [What causes Post-traumatic Headaches & how does it tie into Multi-modal deep learning?](https://youtu.be/r8OLs_1XUvc?t=1904)

### [**Intuition behind how Shapley Values Work**](https://youtu.be/r8OLs_1XUvc?t=1991)
* [How to connect Shapley Values](https://youtu.be/r8OLs_1XUvc?t=2097)

### [**Future**](https://youtu.be/r8OLs_1XUvc?t=2192)

### [**Podcast**](https://youtu.be/r8OLs_1XUvc?t=2276)

## Q&A

1. [Are models for financial credits or insurance price justifiable??](https://youtu.be/r8OLs_1XUvc?t=2493)

1. [Ethics in AI](https://youtu.be/r8OLs_1XUvc?t=2552)

1. [Are there methods to avoid bias before training?](https://youtu.be/r8OLs_1XUvc?t=2675)

1. [Shapley Values: Remove 2 features at once?](https://youtu.be/r8OLs_1XUvc?t=2766)

1. [Is there any *gray box* model in ML?](https://youtu.be/r8OLs_1XUvc?t=2838)