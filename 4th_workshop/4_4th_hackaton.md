# [4th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-04/) 
## Speakers: Rain Wu, Jacob Chen, Rick Tu, Jose Chang
## Title: 4th Hackathon

Takeaways:

* Brief history of Object Detection Algorithms
* Face Detection and social distance demonstration

Sections of the talk:

### **Object Detection**
* [What is the object and its location in the image?](https://youtu.be/9q8ubCHCW4M?t=411)
* [Object Detection Approaches](https://youtu.be/9q8ubCHCW4M?t=569)
  
### Sliding Window
* [Selecting a region](https://youtu.be/9q8ubCHCW4M?t=629)
  * Computationally expensive, slow --> **not** viable

### Region Based CNN
* [Select a region of interest](https://youtu.be/9q8ubCHCW4M?t=815) 
* [Selective search approach algorithm](https://youtu.be/9q8ubCHCW4M?t=859)
* Factors
    * Color
    * Texture
    * [Hierarchical structure (car example)](https://youtu.be/9q8ubCHCW4M?t=1004)
* [R-CNN pipeline scheme](https://youtu.be/9q8ubCHCW4M?t=1253)
* [Drawback of R-CNN](https://youtu.be/9q8ubCHCW4M?t=1290)
* [Improved Versions of R-CNN](https://youtu.be/9q8ubCHCW4M?t=1316)
    * Fast R-CNN
    * Faster R-CNN
    * Mask R-CNN

### YOLO: You only look once
* [Brief history of YOLO](https://youtu.be/9q8ubCHCW4M?t=1398)
* [Advantage of YOLO over R-CNN](https://youtu.be/9q8ubCHCW4M?t=1415)
* [YOLO flowchart](https://youtu.be/9q8ubCHCW4M?t=1441)
* [YOLO working principle: S x S grid](https://youtu.be/9q8ubCHCW4M?t=1497)

### Single Shot  Multibox Detector
* [Pyramidal feature hierarchy method & its advantages over YOLO](https://youtu.be/9q8ubCHCW4M?t=1703)
* [SSMD architecture](https://youtu.be/9q8ubCHCW4M?t=1826)
* [Performance comparison with other methods](https://youtu.be/9q8ubCHCW4M?t=1963)
 
### **Code 1: Face Detection**
* [Pipeline explanation](https://youtu.be/9q8ubCHCW4M?t=2202)
### Coding Pipeline
  * [Create color camera](https://youtu.be/9q8ubCHCW4M?t=2291)
    * [`.setInterLeave(False)` What does it mean?](https://youtu.be/9q8ubCHCW4M?t=2666)
  * [Resize image(500x500)-->(300x300)](https://youtu.be/9q8ubCHCW4M?t=2421)
  * [Import Face Detection Model](https://youtu.be/9q8ubCHCW4M?t=2524)
  * [Link Elements](https://youtu.be/9q8ubCHCW4M?t=2764)
  * [Start Pipeline](https://youtu.be/9q8ubCHCW4M?t=2872)
  * [Setting up queue](https://youtu.be/9q8ubCHCW4M?t=2921)
### Post-processing
  * [How to get detections? Why `.tryGet()` instead of `.get()`?](https://youtu.be/9q8ubCHCW4M?t=2966)
  * [Plot detections](https://youtu.be/9q8ubCHCW4M?t=3126)
### **Code 2: Social Distance**
#### Modification to Pipeline
* [Pipeline Explanation](https://youtu.be/9q8ubCHCW4M?t=3453)
* [Create and link left & right mono cam objects for depth calculation](https://youtu.be/9q8ubCHCW4M?t=3636)
* Update Face Detection model to include depth
    * `.createMobileNetDetectionNetwork()` --> `.createMobileNetSpatialDetectionNetwork()`
* [Display face spatial location](https://youtu.be/9q8ubCHCW4M?t=3736)
    * [` .spatialCoordinates `](https://youtu.be/9q8ubCHCW4M?t=3775) contains x, y,or z position relative to camera
* [Demonstration in x direction](https://youtu.be/9q8ubCHCW4M?t=3885)
*  [Spatial location calculated as an average of all pixel confoming face](https://youtu.be/9q8ubCHCW4M?t=4163)
#### Calculate distance between faces
*  [Iterate through detection in order to compare them](https://youtu.be/9q8ubCHCW4M?t=4253)
*  [What is a symmetrical matrix?](https://youtu.be/9q8ubCHCW4M?t=4325)
    *  [Why only below diagonal elements?](https://youtu.be/9q8ubCHCW4M?t=4368)    `if i < j: `
*  [Distance between two points](https://youtu.be/9q8ubCHCW4M?t=4437)
*  [Append distances to an empty array](https://youtu.be/9q8ubCHCW4M?t=4637)
*  [Set a min. trigger distance ~ 50cm](https://youtu.be/9q8ubCHCW4M?t=4678)
*  [Make line between detections](https://youtu.be/9q8ubCHCW4M?t=4750)
    *  [Setting points for line ](https://youtu.be/9q8ubCHCW4M?t=4760)
    *  [Set color to change if min distance triggered](https://youtu.be/9q8ubCHCW4M?t=4996)
    *  [Display line](https://youtu.be/9q8ubCHCW4M?t=5029)
*  [Display actual calcualted distance between faces](https://youtu.be/9q8ubCHCW4M?t=5090)
*  [Final Result](https://youtu.be/9q8ubCHCW4M?t=5220)
