# README #

This repository contain additional material related to the [OnCV&AI workshops](https://www.nordlinglab.org/workshops/), including our hackathon material and code.

All material is arranged in folders containing the number of the workshop.

## Hackathon material and notes ##

### 1st OnCV&AI workshop ###

In week 1 we will discuess about the DepthAI and the camera (OAK-D) control:

*  DepthAI
*  Camera control
*  Multiple device pre-Host
*  Data collection using OAK-D

### 2nd OnCV&AI workshop ###

In week 2 we will discuess about data collection part of our sbumission for Opencv AI competition:

*  Introduction to PD
*  Quantification of motor functions of PD parients
*  Method of Quantify motion
*  Result

### 3rd OnCV&AI workshop ###

**Our code for training a CNN is available [in a Jupyter notebook at Google Colab](https://colab.research.google.com/drive/1M8TLyetmGPqUvZm_MGfjVWkeiby1becs?usp=sharing)**

In week 3 we will discuess about CNN part of our submission for the Opencv AI competition:

*  Introduction to deep learning
*  How the CNN work
*  Result

### 4th OnCV&AI workshop ###

In week 4 we will discuess about using the AI model in the OAK-D with DepthAI and application:

*  Introduction to AI model for Object detection
*  Using SSD face detection in the the OAK-D
*  Create a social distance example with face detection model

### 5th OnCV&AI workshop ###

In week 5 we will discuess about pose estimation and the OpenPose method:

*  Introduction to pose estimation
*  Top-down and Bottom-up training approach 
*  Introduction of OpenPose
*  Understand the post-processing part of the OpenPose method