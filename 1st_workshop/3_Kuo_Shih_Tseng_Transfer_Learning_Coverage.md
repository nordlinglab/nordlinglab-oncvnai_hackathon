# [1st online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-01/)
## Speaker: Prof. Kuo-Shih Tseng, Assistant professor at Central University.
## Title: [Transfer learning of coverage functions via invariant properties in the Fourier domain](https://www.nordlinglab.org/workshop-oncvai-01/)

Take away:

* For mapping environment 

Sections of the talk:
### [**Coverage Function Explanation**](https://youtu.be/yds3H4nCo40?t=189)

* How Coverage Function works (Search via "Spatial Fourier Sparse Set")
* Relation Spatial and Fourier Domain
* Overlapping & Sparcity 
### [**Sparsity**](https://youtu.be/yds3H4nCo40?t=369)
* influence of sets and environment in the sparsity of coverage functions 
  
### **Theorem and Corollaries** 
* [Theorem 1: Sum of sets intersections](https://youtu.be/yds3H4nCo40?t=455)
* [Corollary 2: Sparsity ](https://youtu.be/yds3H4nCo40?t=531)
* [Corollary 3: higher order --> lower Fourier Coeff. ](https://youtu.be/yds3H4nCo40?t=599)
* [Corollary 4 & 5: Fourier support  & Compression Ratio](https://youtu.be/yds3H4nCo40?t=638)
* [Corollary 6: Maximum number of Fourier support  --> empty map ](https://youtu.be/yds3H4nCo40?t=701)
* [Theorem & Corollary Conclusions](https://youtu.be/yds3H4nCo40?t=765)

### **Algorithms** 
* [Fourier Support Generator](https://youtu.be/yds3H4nCo40?t=817)
* [Spatial Fourier Sparse Set (SFSS) Learning Algorithm](https://youtu.be/yds3H4nCo40?t=884)
* [Search Algorithm](https://youtu.be/yds3H4nCo40?t=921)

### [**Experiment setups and Results**](https://youtu.be/yds3H4nCo40?t=16m45s)


### [**Highlight**](https://www.youtube.com/watch?v=yds3H4nCo40?start=840&end=870)
  