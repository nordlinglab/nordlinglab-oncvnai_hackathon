# [1st online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-01/)
## Speaker: Shih-wei Liao 
## Worked at 
* Standford University
* Intel 
* Google - Recieved Google's Founder Award
## Title: Certificateless Agregate Signature (CLAS) for IoT. 
  

### [**PLTT Blockchain**](https://youtu.be/BXw7pg_b2OU?t=347)
* Production Logistics Tracing & Tracking Blockchain
* A blockchain for tracing and tracking products
* What criteria must be meet for this tech to be applicable?
* Example: Tracking Water contamination ??
  
### [**Data and metrics about PLTT BlockChain**](https://youtu.be/BXw7pg_b2OU?t=556)
* number of users
* number of transaction
* number of chains

### [**IoT Blockchain**](https://youtu.be/BXw7pg_b2OU?t=764)
  * Implement more in Agriculture and Food Safety


### [**System Architecture**](https://youtu.be/BXw7pg_b2OU?t=1048)
* System Operations and their performance
* System Layout
* Advantages of CLAS and its characteristics  
### [**System Operations and their performance**](https://youtu.be/BXw7pg_b2OU?t=1221)
* 

### [**Blockchain Triangle**](https://youtu.be/BXw7pg_b2OU?t=1324)
* Blockchain  Triangle : **Scalability-Security-Decentralization**  (impossible to have all three)
* Explanation on how CLAS system optimizes to provide most of the three

### [**Implementation with society**](https://youtu.be/BXw7pg_b2OU?t=1527)
* example: Contracts between farmers and banks

## Q&A

1. [Multi-layers blockchain](https://youtu.be/BXw7pg_b2OU?t=1926)


1. [Blockchain:  2-part key generation](https://youtu.be/BXw7pg_b2OU?t=2514)


1. [Practical use for customers](https://youtu.be/BXw7pg_b2OU?t=2984)

## [Highlight](https://www.youtube.com/watch?v=BXw7pg_b2OU)
  