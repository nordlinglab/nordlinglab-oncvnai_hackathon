# [1st online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-01/)
## Speaker: Brandon Gilles, CEO of Luxonis 
## Title: Commute Guardian – DepthAI’s North Star
Takeaways:

* Get to know about the Commute Guardian, a device keep cyclist safe from distracted drivers 

Sections of the talk: 
### [**Company Mission**](https://youtu.be/zHZqDOiXvvk?t=140)
* facilitate the implementation of "Embedded Performant Spatial AI and CV into products " and projects to the general public
* use the aformentioned technologies to protect cyclist from distracted automobile drivers
   
### [**Loud Bicycle Example**](https://youtu.be/zHZqDOiXvvk?t=404)
* Cyclist not constantly looking back 
* Problem with this approach: cyclist's awareness or lack thereof to incoming cars strongly influences accident avoidance success rate

  

### [**Proposed Solution**](https://youtu.be/zHZqDOiXvvk?t=714)
* An always on, self-aware prevention system independent from cyclist
* Such system can be made through the integration of the following: 
    * Environmment sensing (radar, stereo cameras)
    * Artificial intelligence
    * Computer Vision


### [**Myriad X Chip**](https://youtu.be/zHZqDOiXvvk?t=940)
* Myriad X is a powerful chip (large datapath) that can handle high data transfer perfect for performing the previously mentioned tasks

### [**Depth AI Platform**](https://youtu.be/zHZqDOiXvvk?t=1038)
* *depth AI OAK Platform* makes it easy for users to setup AI, CV, and enviroment sensing tech into their projects

### [**Spatial AI & CV**](https://youtu.be/zHZqDOiXvvk?t=1128)
* explanation of spatial AI, CV and their integration
* mentions importance of stereo information and techniques such as semantic segmentation


### [**Advantages of Embedded Systems**](https://youtu.be/zHZqDOiXvvk?t=1269)
* better power performance at smaller size and lower power

### [**Applications**](https://youtu.be/zHZqDOiXvvk?t=1550)
* help visually impaired
* restrict scooter velocity in the vicinity of pedestrians
* health & safety systems

### [**AI techniques demonstration**](https://youtu.be/zHZqDOiXvvk?t=1796)
* object detection
* focus on region of interest instead of whole camera input
* edge detection 
* feature tracking
* motion estimation

### [**Commute Guardian Test**](https://youtu.be/zHZqDOiXvvk?t=1974)
* Commute Guardian early test on a bicycle. 
    * depth view demonstration

## Q&A
 
1. [Hard Synchronization Speed](https://youtu.be/zHZqDOiXvvk?t=2272)


1. [Future Optimizations](https://youtu.be/zHZqDOiXvvk?t=2400) 
    * not to run all functions at the same time, instead have a more power efficient configuration 
    * extended battery life

1. [Capacity limitations of Commute Guardian](https://youtu.be/zHZqDOiXvvk?t=2622)


1. [Chip's power configurations](https://youtu.be/zHZqDOiXvvk?t=3211)
    * The power can vary a lot depending on what parts of the chip are being used
    * the chip offers flexibility to turn on or off different sections of the chip
  
1. [Using Myriad X for video Surveillance](https://youtu.be/zHZqDOiXvvk?t=3307)
    * Overkill for stationary camera systems, moving systems take full advantage of the chip's capabilities



### [**Highlight**](https://youtu.be/zHZqDOiXvvk?t=1565)