import cv2
import depthai as dai
import contextlib

def createRGBPipeline():
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(640,400)
    xout_rgb = pipeline.createXLinkOut()
    xout_rgb.setStreamName('rgb')
    cam_rgb.preview.link(xout_rgb.input)
    print('Created camera object')
    print('Created pipeline')
    return pipeline

pipeline = createRGBPipeline()
q_rgb_list = []
with contextlib.ExitStack() as stack:
    for device_info in dai.Device.getAllAvailableDevices():
        device = stack.enter_context(dai.Device(pipeline, device_info))
        print('Connected to '+str(device_info.getMxId()))
        device.startPipeline()

        q_rgb = device.getOutputQueue(name = 'rgb', maxSize = 4, blocking=False)
        q_rgb_list.append(q_rgb)

    while True:
        for i,q_rgb in enumerate(q_rgb_list):
            in_rgb = q_rgb.get()
            rgb_frame = in_rgb.getCvFrame()
            cv2.imshow('rgb'+str(i), rgb_frame)

        if cv2.waitKey(1)==ord('q'):
            break
