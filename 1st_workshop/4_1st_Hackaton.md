# [1st online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-01/) 
## Speakers: Rain Wu, Jacob Chen, Rick Tu, Jose Chang
## Title: [Intro to OAK-D Camera and Depth AI](https://www.nordlinglab.org/workshop-oncvai-01/)

Takeaways:

* Setting up OAK-D camera for CV operations using DepthAI library.
* Building a pipeline and finding the depth.

 
Sections of the talk:
### [**OAK-D hardware specs**](https://youtu.be/S3k7qeXsfz4?t=289)


### [**Key Concepts**](https://youtu.be/S3k7qeXsfz4?t=411)
* What is Depth AI? 
* What does CV and spatial AI mean and what do these include?
  
### [**DepthAI Workflow**](https://youtu.be/S3k7qeXsfz4?t=753)
* Import DepthAI
* Define pipeline
* Start pipeline
* Data Acquisition
* Post process data
  
### [**Camera Operating modes**](https://youtu.be/S3k7qeXsfz4?t=1069)

  * Data Collection
  * Inference
  * Development

### [**Installation**](https://youtu.be/S3k7qeXsfz4?t=1199)
* Connecting the OAK-D to your system (computer) 
* [How to set up a virtual environment and install package](https://youtu.be/S3k7qeXsfz4?t=1320)
* [Why the need for a virtual environment?](https://youtu.be/S3k7qeXsfz4?t=1482)
* [Install DepthAI](https://youtu.be/S3k7qeXsfz4?t=1590)
* [Install OpenCV](https://youtu.be/S3k7qeXsfz4?t=1635)
* [Installation Check](https://youtu.be/S3k7qeXsfz4?t=1668)

### [**Script to get Data from the cameras (mono left, mono right, and RGB) in the OAK-D**](https://youtu.be/S3k7qeXsfz4?t=1809)
* [Relationship between Disparity and Depth](https://youtu.be/S3k7qeXsfz4?t=2021)
* [Defining Pipeline](https://youtu.be/S3k7qeXsfz4?t=2162)
* [Start Pipeline](https://youtu.be/S3k7qeXsfz4?t=2888)
* [`.getOutoutQueue` Data Parameters: MaxSize, blocking](https://youtu.be/S3k7qeXsfz4?t=3026)
  
### [**Getting Data From Queue**](https://youtu.be/S3k7qeXsfz4?t=3189)
* How to manage DepthAI image frame
* Difference between RGB and Monochromatic camera output data
* [Demonstration Disparity from Monochromatic Cameras](https://youtu.be/S3k7qeXsfz4?t=3660)
* [Disparity Calculatiom: Min & Max Values](https://youtu.be/S3k7qeXsfz4?t=3778)
* [Normalization of Disparity Map](https://youtu.be/S3k7qeXsfz4?t=3886)
* Tweaking Disparity map:
    * [`.setExtendedDisparity(True)`](https://youtu.be/S3k7qeXsfz4?t=4046)
    * [`.setLeftRightCheck(True)`](https://youtu.be/S3k7qeXsfz4?t=4317)



### [**Depth & disparity settings in real-life scenario**](https://youtu.be/S3k7qeXsfz4?t=4900)

### [**Connect Multiple Cameras to same host**](https://youtu.be/S3k7qeXsfz4?t=5311)
* [using Exit Stack Function](https://youtu.be/S3k7qeXsfz4?t=5643)
       ```with contextlib.exitStack() as stack:```
      
### [**Encoding Video Data into compressed Format**](https://youtu.be/S3k7qeXsfz4?t=6157)

* [Enconding Process Explanation](https://youtu.be/S3k7qeXsfz4?t=6232)
* [Compression Formats](https://youtu.be/S3k7qeXsfz4?t=6322)
* [Creating Camera Objects](https://youtu.be/S3k7qeXsfz4?t=6408)
* [Creating Video Encoders](https://youtu.be/S3k7qeXsfz4?t=6686)
* [Linking objects](https://youtu.be/S3k7qeXsfz4?t=6995)
* [Get Queue Data](https://youtu.be/S3k7qeXsfz4?t=7349)
* [Saving Data](https://youtu.be/S3k7qeXsfz4?t=7442)
* [Converting Data to playable format](https://youtu.be/S3k7qeXsfz4?t=8067)
* [Max Resolution & FPS](https://youtu.be/S3k7qeXsfz4?t=8206)
    
    
    

