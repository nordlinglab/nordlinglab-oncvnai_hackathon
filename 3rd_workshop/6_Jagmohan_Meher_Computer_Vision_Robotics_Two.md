# [3rd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/)
## Speaker: Jagmohan Meher, Heading the Research and Development department of Skyfi Labs
## Topic: [Computer Vision in Robotics & Teaching OpenCV to Beginner](https://www.nordlinglab.org/workshop-oncvai-03/#RJM)

Takeaways:

* Use OpenCV to detect objects according to color


### [**Tesla autopilot example**](https://youtu.be/N3lLSHrJBH0?t=402)

### [**Pipeline for Obj Detection**](https://youtu.be/N3lLSHrJBH0?list=TLPQMTgwOTIwMjFMsUR555ek8g&t=532)

1. ### [Image Acquisition](https://youtu.be/N3lLSHrJBH0?t=600)
   *  ``` cam = cv.VideoCapture(0)```
   * ```ret, frame = cam.read()```


1. ### [Preprocessing: "Smoothing the image"](https://youtu.be/N3lLSHrJBH0?t=759)

    * [Gaussian Blur:](https://youtu.be/N3lLSHrJBH0?t=866)
    `cv2.GaussianBlur(img,ksize,sigma)`

    * [Types of Blurring (OpenCV documentation)](https://youtu.be/N3lLSHrJBH0?t=912)

1. ### [Color Recognition: BGR to HSV](https://youtu.be/N3lLSHrJBH0?t=1084)

    * [Thresholding](https://youtu.be/N3lLSHrJBH0?t=1227)

1. ### Object Identification: Contour
   * [find object contour](https://youtu.be/N3lLSHrJBH0?t=1570)
   * [find max area](https://youtu.be/N3lLSHrJBH0?t=1831)
   * [bounding box](https://youtu.be/N3lLSHrJBH0?t=1996)
  
## Q&A

1. [How to import libraries?](https://youtu.be/N3lLSHrJBH0?t=2484)


1. [What does 10 mean in the code below?](https://youtu.be/N3lLSHrJBH0?t=2509)
    key=cv.waitKey(10)

1. [Why do we need RGB if we have HSV?](https://youtu.be/N3lLSHrJBH0?t=2541)

1. [Why do we need Gaussian Smoothing?](https://youtu.be/N3lLSHrJBH0?t=2587)

1. [Is it possible to get more uniform color on the ball? Will this improve results?](https://youtu.be/N3lLSHrJBH0?t=2632)

1. [Speakers previous projects using taught techniques](https://youtu.be/N3lLSHrJBH0?t=2684)

1. [How to estimate the improvement of smoothing vs no smoothing?](https://youtu.be/N3lLSHrJBH0?t=2818)

1. [How to adjust color threshold depending on lighting conditions?](https://youtu.be/N3lLSHrJBH0?t=2879)


