# [3rd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/) 
## Speaker: Prof. Wei-Chun Chiang, Assistant Professor at EE in NCKU
## Title: [A food image recognition system for school lunch using deep learning neural network](https://www.nordlinglab.org/workshop-oncvai-03/#RWCC)

Takeaways: 

* Identifying issues in your model and making adjustments

Sections of the talk:

### [**Where does the data come from?**](https://youtu.be/g16YwHTppTo?t=240)
* Smart Campus Catering Service Platform ( A Database is school lunch food pics)
* [Data Collection App](https://youtu.be/g16YwHTppTo?t=283)

### [**Food Image Recognition**](https://youtu.be/g16YwHTppTo?t=353)
* [Optimizer: Sharpness-Aware Minimization (SAM)](https://youtu.be/g16YwHTppTo?t=379)
  
### [**Data Characteristics & Pre-processing**](https://youtu.be/g16YwHTppTo?t=432)


### [**Food Classifier Model iterations**](https://youtu.be/g16YwHTppTo?t=531)
* [Problem & Workaround](https://youtu.be/g16YwHTppTo?t=745)

### [**Results & Conclusions**](https://youtu.be/g16YwHTppTo?t=811)

## Q&A

1. [Is it possible to use yolo model to improve green vegetable clasification?](https://youtu.be/g16YwHTppTo?t=935)

1. [Under what conditions does the model perform best (brightness, lunchbox container etc..)](https://youtu.be/g16YwHTppTo?t=983)

1. [What can be done to determine if the food in the pic has a good nutritional value](https://youtu.be/g16YwHTppTo?t=1056)

1. [Can the app suggest better dietary options?](https://youtu.be/g16YwHTppTo?t=1137)

1. [Can the model classify other type of foods?](https://youtu.be/g16YwHTppTo?t=1210)


