# [3rd Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/)
## Speaker: Anwaar Ulhaq, Lecturer and Deputy Leader of Machine Vision and Digital Health Research at Charles Sturt University in Australia
## Title: [Use of Computer Vision for Environment Protection](https://www.nordlinglab.org/workshop-oncvai-03/#SAU)

Highly recommended by Kung - "This talk contains many pictures that makes it easy to understand"

Sections of the talk:
### **Introduction**
 * [Smart City](https://youtu.be/IEn8f0pgY2E?t=176)
 * [Advent of AI](https://youtu.be/IEn8f0pgY2E?t=273)
 * [Current State of Computer Vision](https://youtu.be/IEn8f0pgY2E?t=343)
   * Semantic Segmentation

### [**Integration of CV & AI to Cloud Computing**](https://youtu.be/IEn8f0pgY2E?t=427)
* [What is Edge Computing](https://youtu.be/IEn8f0pgY2E?t=495) 
* [Addressing Latency](https://youtu.be/IEn8f0pgY2E?t=521)

### [**Devices used for Edge Computing**](https://youtu.be/IEn8f0pgY2E?t=605)
    

### [**Computer Vision for Ecology**](https://youtu.be/IEn8f0pgY2E?t=684)
1. [Real Time Plant Disease Diagnosis](https://youtu.be/IEn8f0pgY2E?t=752)
      * [CNN Model](https://youtu.be/IEn8f0pgY2E?t=909)
        * Aim: lightweight model for end device
      * [System Work-flow](https://youtu.be/IEn8f0pgY2E?t=909)
      * [Performance Comparison](https://youtu.be/IEn8f0pgY2E?t=1040)
1. [Animal classifier & counter from drone thermal video](https://youtu.be/IEn8f0pgY2E?t=1093)
    * [Dilated Convolution & Model Architecture](https://youtu.be/IEn8f0pgY2E?t=1339)
    * [Video demonstration](https://youtu.be/IEn8f0pgY2E?t=1449)
2. [Vegetation Health Assesment: Evaluating Biomass from Images](https://youtu.be/IEn8f0pgY2E?t=1625)
    * [Alignment technique for pictures of the same area from different angles](https://youtu.be/IEn8f0pgY2E?t=1790)
    * [Comparing same trees from different video inputs: Contrastive Loss Funtion](https://youtu.be/IEn8f0pgY2E?t=2030)
    * [Model Framework](https://youtu.be/IEn8f0pgY2E?t=2093)