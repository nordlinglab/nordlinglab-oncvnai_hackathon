# [3rd Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/)
## Speaker : Che-Wei Lin, Assistant Professor at the National Cheng Kung University in Taiwan
## Title: [AI-assisted Orthopedics—Development of an Explainable XCuffNet for Rotator Cuff Tear Radiography Classification](https://www.nordlinglab.org/workshop-oncvai-03/#RCWL) 
Takeaways:

* Viability of AI for injury diaganosis against conventional methods

Sections of the talk:
### [**Rotator cuff tear (RTC) injury**](https://youtu.be/OnHVl7SJkHo?t=312)
* [Current Diagnosis Methods](https://youtu.be/OnHVl7SJkHo?t=364)
  * MRI: accurate, but expensive and time consuming
  * radiography: less accurate, but inexpensive and faster results
  * Aim: improve diagnosis accuracy with radiaography + CV and AI.

### [**Important Shoulder Radiography Features for Diagnosis** (***not*** CNN extracted Features)](https://youtu.be/OnHVl7SJkHo?t=443) 

### [**Research Objective**](https://youtu.be/OnHVl7SJkHo?t=565)
* Try both *Deep Learning* and *Machine Learning* algorithms 

### [**Algorithm Flowchart**](https://youtu.be/OnHVl7SJkHo?t=670)

### [**Deep Learning Approach**](https://youtu.be/OnHVl7SJkHo?t=788)

* [Image preparation](https://youtu.be/OnHVl7SJkHo?t=795)
* [CNN model Architecture](https://youtu.be/OnHVl7SJkHo?t=888)
  * [1st & 3rd conv layers, responsible for low and high level feature extraction](https://youtu.be/OnHVl7SJkHo?t=963)
* [Comparison with other well establish CNN models](https://youtu.be/OnHVl7SJkHo?t=991)
* [What is Feature Visualization?](https://youtu.be/OnHVl7SJkHo?t=1044)
  
### [**Evaluation Metrics of Deep Learning Approach**](https://youtu.be/OnHVl7SJkHo?t=1129)
* [Performance according to ROC & Confusion Matrix](https://youtu.be/OnHVl7SJkHo?t=1221)
* [Image Feature Visualization Results](https://youtu.be/OnHVl7SJkHo?t=1251)

### [**Machine Learning Approach**](https://youtu.be/OnHVl7SJkHo?t=1319)
* [Feature Selection Methods & model classifiers](https://youtu.be/OnHVl7SJkHo?t=1377)
  * [Best Features for Selection](https://youtu.be/OnHVl7SJkHo?t=1420)
* [Machine Learning Results](https://youtu.be/OnHVl7SJkHo?t=1444)


### [**Results Comparison: Deep Learning vs Machine Learning**](https://youtu.be/OnHVl7SJkHo?t=1634)
* Deep Learning vs Machine Learning Performance Comparison


## Q&A


1. [Why does XCuffNet have better performance](https://youtu.be/OnHVl7SJkHo?t=2367)

1. [What is the output of XCuffNet??](https://youtu.be/OnHVl7SJkHo?t=2468)

1. [Automatic Extraction methods to increase efficiency](https://youtu.be/OnHVl7SJkHo?t=2554)

1. [Is a 2% difference in accuracy significant?](https://youtu.be/OnHVl7SJkHo?t=2611)

1. [Does cropping make it easier to focus on a region of interest?](https://youtu.be/OnHVl7SJkHo?t=2701)