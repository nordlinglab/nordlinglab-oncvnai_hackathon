# [3rd Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/) 
## Speakers: Rain Wu, Jacob Chen, Rick Tu, Jose Chang
## Title: [3rd Hackathon](https://www.nordlinglab.org/workshop-oncvai-04/#SHT)

Takeaways:

* Learn what is the process behind a Convolution
* How to build a 3-D CNN to predict finger tapping velocity & position

Sections of the talk:


### [**Deep Learning and related terms**](https://youtu.be/mMNzConFRP0?t=296)
* [Machine Learning vs Machine Learning](https://youtu.be/mMNzConFRP0?t=571)
* [What is a statistical model](https://youtu.be/mMNzConFRP0?t=860)
* [Typical Deep Neural Network Architecture](https://youtu.be/mMNzConFRP0?t=1018)
  * [Why the need for non-linearity?](https://youtu.be/mMNzConFRP0?t=1245)
* [Typical Deep Learning Model Pipeline](https://youtu.be/mMNzConFRP0?t=1355)
  * [Types of Deep learning](https://youtu.be/mMNzConFRP0?t=1412)
  * [Types of Model structures and their applications](https://youtu.be/mMNzConFRP0?t=1497)
  * [CNN typical learning process](https://youtu.be/mMNzConFRP0?t=1608)

### [**ImageNet Competition**](https://youtu.be/mMNzConFRP0?t=1914)
* [How to tweak model to obtain good accuraccy?](https://youtu.be/mMNzConFRP0?t=2400)
  
### **Convolution**
* [How does the **"Convolution"** operation work?](https://youtu.be/mMNzConFRP0?t=2634)

### [**Method**](https://youtu.be/mMNzConFRP0?t=3034)
* [Protocol to get the video data](https://youtu.be/mMNzConFRP0?t=3052)
* [Video Preprocessing: Data Manipulation](https://youtu.be/mMNzConFRP0?t=3252)
* [Video Preprocessing: Jackkniffing Resampling Scheme](https://youtu.be/mMNzConFRP0?t=3475)
* [Metrics Normalization](https://youtu.be/mMNzConFRP0?t=3675)
* [Brief explanation of data obtained](https://youtu.be/mMNzConFRP0?t=3722)
* [CNN architecture](https://youtu.be/mMNzConFRP0?t=3936)

### [**Coding Part**](https://youtu.be/mMNzConFRP0?t=4393)
* Preprocessing 
    * [Preprocessing: jackkniffing](https://youtu.be/mMNzConFRP0?t=4440)
    * [Splitting data for *training* and *testing*](https://youtu.be/mMNzConFRP0?t=4758)
    * [normalization (0~1) & open-close classification](https://youtu.be/mMNzConFRP0?t=4860)
* [Model Structure](https://youtu.be/mMNzConFRP0?t=4875)
* [Fiting train labels to test labels](https://youtu.be/mMNzConFRP0?t=5077)
* [Evaluation of model results](https://youtu.be/mMNzConFRP0?t=5158)
    * [Residuals](https://youtu.be/mMNzConFRP0?t=5265)

### [**Results**](https://youtu.be/mMNzConFRP0?t=5500)
* [Number of epochs trained and why](https://youtu.be/mMNzConFRP0?t=5519)
* [Error distribution](https://youtu.be/mMNzConFRP0?t=5660)
* [Predictions for distance, open/close detection, velocity](https://youtu.be/mMNzConFRP0?t=5689)
* [training time vs performance trade off](https://youtu.be/mMNzConFRP0?t=5900)

### [**Conclusions**](https://youtu.be/mMNzConFRP0?t=6162)

## Q&A

1. [What are hyper parameters??](https://youtu.be/mMNzConFRP0?t=6375)

1. [Is there a way to reduce layers in the model to make it lighter?? How does it affect performance?](https://youtu.be/mMNzConFRP0?t=7074)

1. [How to select a suitable activation function?](https://youtu.be/mMNzConFRP0?t=7594)

1. [How to prevent overfitting?](https://youtu.be/mMNzConFRP0?t=7782)