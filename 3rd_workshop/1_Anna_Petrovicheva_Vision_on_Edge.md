# [3rd Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/)
## Speaker: Anna Petrovicheva, CTO at OpenCV.AI
## Title: [Vision on Edge Devices](https://www.nordlinglab.org/workshop-oncvai-03/#SAP)

Takeaways: 

* The process from model training to deployment on edge devices.
* When you import a model to another device, you need to verify that the first four digits of the predictions are the same for your validation dataset.

Sections of the talk:
### [**What are *edge* devices??**](https://youtu.be/EN31gVRdkC0?t=262)
* Physically close to consumer 
* Computation on device
* Examples: smart watch, cellphone
* Pros of Edge Devices:  
    * Light, many running on battery power <--> in contrast to servers
    * Easy to install 
* Downside of Servers: 
    * Expensive (ex. AWS)
    * Reliant on internet for data transfer
    * Security & privacy become issues when dealing with sensitive data
  
### [**2010 to Today : Development of CV algorithms and their consequence towards edge devices**  ](https://youtu.be/EN31gVRdkC0?t=508)
* In a nutshell: As algorithms get more efficient and hardware becomes more powerful, more and more appplications are making use of edge devices
* Chronologically:
    1. Real time CV performing algorithms
    2. Deep learning algoritms started working(heavier --> only viable to be run in servers)
    3. Development of specialized hardware of CV
    4. Prioritization of speed over accuracy (GOOGLE for smarphones)

 
### [**How are CV algorithms migrating towards edge devices**](https://youtu.be/EN31gVRdkC0?t=952)

#### Hardware: Two main Reasons
  1. [In-chip Matrix Multiplication Unit](https://youtu.be/EN31gVRdkC0?t=995)
  2. [Optimized Data Tranfer within hardware](https://youtu.be/EN31gVRdkC0?t=1078)
     * Bigger Memmory/Chache
     * Faster Data Transfer (ex. OpenCV.AI kit)
        * Intel Neural Compute Stick vs OAk-D data transfer comparison

#### Software - Tools
* [From CV algorithm to target device Process: ***Training--> ONNX-->Inference***](https://youtu.be/EN31gVRdkC0?t=1190)
     1. Training Frameworks: Pythorch, Tensorflow (usually in Python)
     2. ONNX (Open Neural Network Exchange Format)
     3. Edge Frameworks: (usually in C++)
        * Ex: TFLite, OpenVINO, core ML, TensorRT
        
    * Keynote: some frameworks are good for training, others are good for inference   
* [What is ONNX and why is it important??](https://youtu.be/EN31gVRdkC0?t=1409)
  * It is a standard format to simplify conversion among different frameworks

  * [***Training--> ONNX***](https://youtu.be/EN31gVRdkC0?t=1532)
    * NO dynamic shapes
    * Training adb ONNX results must be same to fourth digit after decimal point
  * [***ONNX-->Inference***](https://youtu.be/EN31gVRdkC0?t=1766)
    * ONNX and Inference results must be the same 
    * [Convert inference pipeline, pre- and post- processing to C++](https://youtu.be/EN31gVRdkC0?t=1874)

#### [Software -  Algorithms: How to optimize NN](https://youtu.be/EN31gVRdkC0?t=1989)
* Redundancy in different types of Networks
* Ways to Optimize Neural Networks:
    1. [Quantization](https://youtu.be/EN31gVRdkC0?t=2081)
         * Training 32-bit float **not compatible** with device (for example 8-bit fixed)
    2. [Pruning](https://youtu.be/EN31gVRdkC0?t=2272)
        * Discarding some channels from the neural network
        * Some heuristics to choose:
            1. smallest mean
            2. discard from sets of linearly dependent channels
    1. [Start with small backbone](https://youtu.be/EN31gVRdkC0?t=2392)
           * Find sweet spot between accuracy & performance

### [**Next Five Years**](https://youtu.be/EN31gVRdkC0?t=2536)
* Optimization of current algorithms will lead to higher quality and preformance solutions, or
* New algorithms will be developed, so we will have to rely on servers for a couple of years before these can be inplemented in industry


## Q&A

1. [What was the battery powered device that could run the NN for 4 years??](https://youtu.be/EN31gVRdkC0?t=2709)

1. [Difference between *Scientific* real time & *Real* real time](https://youtu.be/EN31gVRdkC0?t=2767)

1. [How much faster is OAK-D by bypassing limitation of usb cable??](https://youtu.be/EN31gVRdkC0?t=2818)

1. [How much faster is OAK-D by bypassing limitation of usb cable??](https://youtu.be/EN31gVRdkC0?t=2818)

1. [4 digit precision metric](https://youtu.be/EN31gVRdkC0?t=2875)

1. [How to optimize convolution by not moving data back and forth in the memory??](https://youtu.be/EN31gVRdkC0?t=2967)