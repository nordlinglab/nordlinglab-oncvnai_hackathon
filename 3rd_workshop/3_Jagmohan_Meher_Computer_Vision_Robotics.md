# [3rd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-03/)
## Speaker: Jagmohan Meher, Heading the Research and Development department of Skyfi Labs
## Topic: [Computer Vision in Robotics & Teaching OpenCV to Beginner](https://www.nordlinglab.org/workshop-oncvai-03/#RJM)

Takeaways:

* Computer vision from the basics 
* Learn  pedagogical tricks to teach complicated topics such as computer vision

Sections of the talk:
### [**Example: Robot Shooting Baskets**](https://youtu.be/S6ZB0Atkh7M?t=421)

### [**What is Computer Vision?**](https://youtu.be/S6ZB0Atkh7M?t=579)
* [Applications](https://youtu.be/S6ZB0Atkh7M?t=608)
    1. Machine Vision
  2. Object Detection & Tracking
  3. Detection of Events
  4. Human-Computer Interaction Devices
  5. Mapping
  6. Autonomous Driving

### [**What is a Computer Vision Pipeline?**](https://youtu.be/S6ZB0Atkh7M?t=910)
1. Image Acquisition
2. Image Processing into a form we can work with
3. Analysis
4. Decision Making (Goal or Target Action)

### [**What is OpenCV?**](https://youtu.be/S6ZB0Atkh7M?t=984)

## **Key Concepts:**

### [**What is and image**](https://youtu.be/S6ZB0Atkh7M?t=1054)
* Pixel
* Intensity: 0-->255
### [**Pixel Reference**](https://youtu.be/S6ZB0Atkh7M?t=1147)
* Array representation & coordinates

### [***Black & White* and *Color Image* representation in computing**](https://youtu.be/S6ZB0Atkh7M?t=1174)
* Black and White: 1 Channel with intensity from 0 --> 255
* Color Images: 3 channel (RGB) with intensities 0 --> 255
* Why 255? 
  * Color channel are given in 8-bit (eight ones in binary) --> [11111111] = 255 in decimal

### [**Color Models**](https://youtu.be/S6ZB0Atkh7M?t=1285)
* RGB
* HVS: Hue Saturation & Brightnesss
  * Cyclical model
  * Also 3 channels 
     1. Hue --> Color
     2. Saturation --> Grey
     3. Brightness --> White

### [**What is a video?**](https://youtu.be/S6ZB0Atkh7M?t=1452)

## **Coding Part:**

### [**Demostration of Data Acquisition: Showing an Image**](https://youtu.be/S6ZB0Atkh7M?t=1544)
    import cv2 as cv
    import numpy as np
    img = cv.imread("file_name.jpg")
    cv.imshow("image_display_name",img)
    cv.waitKey()
    cv.destroyAllWindows()

### [**Image in RGB --> Grayscale & HSV**](https://youtu.be/S6ZB0Atkh7M?t=2016)

```
img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)   
```
### [**Split Image into 3 channels**](https://youtu.be/S6ZB0Atkh7M?t=2211)
```
b, g, r = cv.split(img)
cv.imshow('blue',b)
cv.imshow('green',g)
cv.imshow('red',r)   
```
### [**Capture Video**](https://youtu.be/S6ZB0Atkh7M?t=2455)
```
cam = cv.VideoCapture(0)

while(True):
    ret, frame = cam.read()
    cv.imshow('Frame',frame)

    key=cv.waitKey(10)

    if key==27:
        break

cam.release()
```
   
