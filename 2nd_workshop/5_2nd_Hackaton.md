# [2nd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-02/)
## Hosts: Rain Wu, Jacob Chen, Rick Tu, and Jose Chang
## Topic: [Quatifying Motor Function of Parkinson Disease Patients](https://www.nordlinglab.org/workshop-oncvai-02/#RHT)

Takeaways: 

* What is Parkinson's Disease and how does it affect patients?
* Use of computer vision to quantify finger tapping test

### [**What is Parkinson's Disease**](https://youtu.be/WEYvimhtiJI?t=224)
* [DALY](https://youtu.be/WEYvimhtiJI?t=268)
* [Video demontration of the effects of Parkinson's Disease](https://youtu.be/WEYvimhtiJI?t=457)
* [Current state of Quantification of Parkinson's Disease and its issues](https://youtu.be/WEYvimhtiJI?t=738)
  * [Syndromes of Parkinson's Disease](https://youtu.be/WEYvimhtiJI?t=1190)
  * [Current Methods to quantify PD(sensors, tests, apps,  etc ...)](https://youtu.be/WEYvimhtiJI?t=1241)
  
### [**How to solve aformentioned issues??**](https://youtu.be/WEYvimhtiJI?t=1404)
* [Recreation of finger tapping test](https://youtu.be/WEYvimhtiJI?t=1478)

### [**How to extract data from finger tapping test video?? (Conceptual Explanation)**](https://youtu.be/WEYvimhtiJI?t=1967)

### **Coding Part**
* [Data Acquisition Process: Practial Diagram Explanation](https://youtu.be/WEYvimhtiJI?t=2439)
* Define pipeline
* [What does Image Signal Processor (Isp) scale do?](https://youtu.be/WEYvimhtiJI?t=2682)
    `.setIspScale(1,2)`
  * Diff between ips and preview out
* [Color image with depth map alignment](https://youtu.be/WEYvimhtiJI?t=2785)
* [Start Pipeline & get queue](https://youtu.be/WEYvimhtiJI?t=3372)
* [Post-processing Explanation](https://youtu.be/WEYvimhtiJI?t=3569)
* [Threshold values for colored stickers](https://youtu.be/WEYvimhtiJI?t=3687)
* [Gaussian blur](https://youtu.be/WEYvimhtiJI?t=4524)
* [Hough Circles](https://youtu.be/WEYvimhtiJI?t=4675)
  * [Hough Circles Parameters](https://youtu.be/WEYvimhtiJI?t=4838)
  * [Pair of Cirlces Best Fit](https://youtu.be/WEYvimhtiJI?t=4991)
  * [Bounding Box](https://youtu.be/WEYvimhtiJI?t=5810)
* [Depth Calculation](https://youtu.be/WEYvimhtiJI?t=6284)
  * [Depth ~ Disparity Relationship](https://youtu.be/WEYvimhtiJI?t=6625)
  * [Calculating Distance](https://youtu.be/WEYvimhtiJI?t=7525)
  * [Plotting 2-D & 3-D dsitances between stickers](https://youtu.be/WEYvimhtiJI?t=7833)
* [Demonstration](https://youtu.be/WEYvimhtiJI?t=8088)

## Q&A

1. [Is the euclidean distance calculated from the camera's 2-D plane or in the real world 3-D space?](https://youtu.be/WEYvimhtiJI?t=8088)
