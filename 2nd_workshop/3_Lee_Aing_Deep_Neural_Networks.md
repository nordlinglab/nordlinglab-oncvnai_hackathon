# [2nd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-02/) 
## Speaker: Lee Aing, PhD student at National Chung Cheng University
## Title: [Deep Neural Network on Pose Estimation](https://www.nordlinglab.org/workshop-oncvai-02/#RLA)

Takeaways:

* How to use the pose estimation technique
* Use the refinement-based method to estimate

### [**Current Role & Applications of different types of DNN**](https://youtu.be/QBEbj58sK98?t=172)
* Computer Vision
* Industrial Robotics
* Medical applications
* Public transportation

### [**What is Pose Estimation??**](https://youtu.be/QBEbj58sK98?t=282)
* [Pose Estimation for human body & types of RGB input](https://youtu.be/QBEbj58sK98?t=313)
    * [**1-stage** and **2-stage** Pose Estimation Training Systems](https://youtu.be/QBEbj58sK98?t=417)
    * 2-stage training first calculates a 2-D skeleton from the image. [Two approaches to calculate 2-D skeleton](https://youtu.be/QBEbj58sK98?t=470)
* [Head Pose Estimation](https://youtu.be/QBEbj58sK98?t=555)
    * [Method to estimate Head Pose Estimation: landmark vs free landmark method](https://youtu.be/QBEbj58sK98?t=576)
* [Vehicle Pose Estimation](https://youtu.be/QBEbj58sK98?t=674)
    * Pose Estimation depending on input type
        * [RGB](https://youtu.be/QBEbj58sK98?t=725)
        * [Stereo](https://youtu.be/QBEbj58sK98?t=757)
        * [Lidar](https://youtu.be/QBEbj58sK98?t=797)
* [Object Pose Estimation](https://youtu.be/QBEbj58sK98?t=847)
    * Object Pose Estimation Methods
        * [Direct method](https://youtu.be/QBEbj58sK98?t=877)
        * [Keypoint based method](https://youtu.be/QBEbj58sK98?t=943)
        * [Feature based method](https://youtu.be/QBEbj58sK98?t=1026)
        * [Refinement based method](https://youtu.be/QBEbj58sK98?t=1112)
    * [Typical architecture of Object Pose Estimation Models](https://youtu.be/QBEbj58sK98?t=1164)
        * Encoder/Decoder Modules --> intermediate feature extraction
        * 2-D CNN --> regress output feature
        * Fully connected layers --> predict object pose
    * Types of machine learning used for pose estimation
        * [Supervised learning](https://youtu.be/QBEbj58sK98?t=1272)
        * [Self-supervised learnign](https://youtu.be/QBEbj58sK98?t=1318)
        * [Reinforcement Learning](https://youtu.be/QBEbj58sK98?t=1375)
    * [Proposed characteristics Multiple Instance Object Pose Estimation Model](https://youtu.be/QBEbj58sK98?t=1440)
        * [Experimental Results](https://youtu.be/QBEbj58sK98?t=1570)

### [**Future Research**](https://youtu.be/QBEbj58sK98?t=1686)
* Predict 3-D skeletons in real time
* implement imput from other type of sensors such as thermal or infrared cameras

## Q&A

1. [Top->Down vs Bottom->Up methods performance comparison](https://youtu.be/QBEbj58sK98?t=1810)

1. [How to evaluate 2-D 3-D pose estimation??](https://youtu.be/QBEbj58sK98?t=1859)

1. [LIDAR for autonomous driving system??](https://youtu.be/QBEbj58sK98?t=1967)

1. [Generation & Discrimination relationship in the Object Pose Feature-based Model](https://youtu.be/QBEbj58sK98?t=2084)

1. [Which one of the models for post estimation is most robust??](https://youtu.be/QBEbj58sK98?t=2163)

1. [Can vehicle pose be used in vehicles other than cars?? (Bicyles, Scooters, etc ...)](https://youtu.be/QBEbj58sK98?t=2266)

1. [Single vs Multi stage methods for human pose estimation](https://youtu.be/QBEbj58sK98?t=2438)