# 2nd online Computer Vision & AI Workshop 
## Speaker: Jarmo T. Alander, Professor of Production Automation at University of Vaasa, Finland
## Title: Interactive Evolutionary Design on an Image Processing Pipeline on an FPPGA for a Spectral Camera
Takeaways:

* Field-programmable gate array (FPGA) advantages over CPU's

Sections of  the talk:
### [**Introduction**](https://youtu.be/9sHBhuLLZoA?list=TLPQMDkwOTIwMjGpJHaLEfMRnA&t=154)
* [Difference between FPGA and Processor](https://youtu.be/9sHBhuLLZoA?list=TLPQMDkwOTIwMjGpJHaLEfMRnA&t=381)
  * [Advantages and disadvanted of FPGA](https://youtu.be/9sHBhuLLZoA?t=424)

### [**Pipeline Power Efficiency**](https://youtu.be/9sHBhuLLZoA?t=486)
* moving data is the most energy consuming operation
* the pipeline minimizes the back and forth movement of data

### [**How do handle mathematical operations to improve efficiency?** ](https://youtu.be/9sHBhuLLZoA?t=608)
* limit circuit according to amount of bits needed
* [Multiply  by contant trick](https://youtu.be/9sHBhuLLZoA?t=699)

### [**CPU vs GPU vs FPGA Energy Comsumption Comparison**](https://youtu.be/9sHBhuLLZoA?t=917)
* FPGA generally better

### [**SoC-FPGA**](https://youtu.be/9sHBhuLLZoA?t=1021)
* FPGA - CPU hybrid

### [**Dark Silicon**](https://youtu.be/9sHBhuLLZoA?t=1088)
* Unused portion of silicon in semiconductor and their effect on heat generation throughout the chip

### [**Genetic Algorithms (GA) Gui**](https://youtu.be/9sHBhuLLZoA?t=1153)
* uses GA to produce pixel RGB representation 
* [Gui Parameters](https://youtu.be/9sHBhuLLZoA?t=1274)
* [Pixel mathematical expression syntax example](https://youtu.be/9sHBhuLLZoA?t=1375)

### [**Results**](https://youtu.be/9sHBhuLLZoA?t=1433)
  * Red Amiral Butterfly
  * Nile- a fluorecesnt due
  * [System Verilog](https://youtu.be/9sHBhuLLZoA?t=1625)

### [**Conclusion & Future Work**](https://youtu.be/9sHBhuLLZoA?t=1694)

## Q&A

1. [Place of SoC-FPGA in Industry](https://youtu.be/9sHBhuLLZoA?t=1807)

1. [Question about features detected in butterfly images](https://youtu.be/9sHBhuLLZoA?t=1913)

1. [Is there a superior Soc for Image Segmentation??](https://youtu.be/9sHBhuLLZoA?t=2056)

