# [2nd online Computer Vision & AI Workshop ](https://www.nordlinglab.org/workshop-oncvai-02/)
## Speaker: Niall O'Mahony,  Working on PhD on AI and Machine Learning on the Field of Agritech
## Title: [Types of Machine Learning Algorithms](https://www.nordlinglab.org/workshop-oncvai-02/#RNO)

Takeaways: 

* Different types of Machine Learning Algorithms 

Sections of the talk:

### [**Intro to types of Machine Learning**](https://youtu.be/UJumvKMwRto?t=146)

### [**Supervised Learning**](https://youtu.be/UJumvKMwRto?t=257)

  * For Regression and Classification Problems
  * [Classification problems: Discriminative vs Generative Models](https://youtu.be/UJumvKMwRto?t=331)
  * [Types of Loss Functions](https://youtu.be/UJumvKMwRto?t=399)
    * Least Squared Error
    * Logistic Loss
    * Hinge Loss
    * Cross-entropy
### **Algorithms (Can be for Classification and Regression)**

* [Linear Regression](https://youtu.be/UJumvKMwRto?t=641)

*   [Logistics Regression](https://youtu.be/UJumvKMwRto?t=821)
*   [Support Vector Machines (use hinge loss)](https://youtu.be/UJumvKMwRto?t=1054)
    *   [Kernels](https://youtu.be/UJumvKMwRto?t=1164)  
*  [Decision Trees](https://youtu.be/UJumvKMwRto?t=1357)
   *  [Random Forests vs Gradient Boosted](https://youtu.be/UJumvKMwRto?t=1752)
* [*k* nearest neighbhors](https://youtu.be/UJumvKMwRto?t=1842)
* [Naive Bayes](https://youtu.be/UJumvKMwRto?t=1992)
* [Deep learning](https://youtu.be/UJumvKMwRto?t=2177)
  * [2-D CNN](https://youtu.be/UJumvKMwRto?t=2574)


### **More Algorithms**

* [Genetic Algorithms (for optimization problems)](https://youtu.be/UJumvKMwRto?t=2652)

* [Reinforcement Learning (learn through experience)](https://youtu.be/UJumvKMwRto?t=2920)

### **Unsupervised Learning**
* [Clustering](https://youtu.be/UJumvKMwRto?t=3037)
* [Dimensionality Reduction: visalization methods](https://youtu.be/UJumvKMwRto?t=3113)
  * [Principal Component Analysis PCA](https://youtu.be/UJumvKMwRto?t=3146)
  * [k means](https://youtu.be/UJumvKMwRto?t=3230)


### [**Highlight**](https://youtu.be/UJumvKMwRto?t=1844)