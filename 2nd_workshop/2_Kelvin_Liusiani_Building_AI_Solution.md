# [2nd online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-02/)
## Speaker : Kelvin Liusiani, CV engineer at VOOX startup company in Singapur


## Title: [Building AI Solution using Edge Computing? Why not?Person Detection on Intel NUC with OpenVINO](https://www.nordlinglab.org/workshop-oncvai-02/#RKL)

Takeaways:

* Difference between synchronous and asynchronous inference
* The iterative process to achieve the desired performance of the people counter system

Sections of the talk: 
### **Introduction**
* [Pros and Cons of High and Low Computational Resource for Object Detection Systems](https://youtu.be/0NldiTtnT_I?t=185)
    * High --> Server
    * Low --> Laptop, Intel NUC  
### [**Affordable, real time people counting system with low computational resouces**](https://youtu.be/0NldiTtnT_I?t=411)
* [Pipeline: Video Feed --> Object Detection --> Tracking --> Counting](https://youtu.be/0NldiTtnT_I?t=525)

### [**What is OpenVINO and what can it do?**](https://youtu.be/0NldiTtnT_I?t=617)
* Can run Pythorch and Tensorflow models
* Can convert from float32 to float16 or int8
* Can perform asynchronous inference

### **Experiments to Improve FPS**

1. [Synchronous vs Asynchronous Inference](https://youtu.be/0NldiTtnT_I?t=671)
    * Synchronous: **1st** inference --> **then** object detection
    * Asynchronous: no fixed pattern for excecution of inference and object detection
    * Synchronous slower as number of frames loaded increases 
2. [Async + Tracking (Sort)](https://youtu.be/0NldiTtnT_I?t=756)
    * Acceptable FPS, Acceptable count error
    * poor tracking 

3. [Async + Tracking (Sort) with improvement 1](https://youtu.be/0NldiTtnT_I?t=818)
    * Change: detection only done every fourth frame
    * Good Result

4. [Async + Tracking (Sort) with improvement 2](https://youtu.be/0NldiTtnT_I?t=995)
     * Change: every 4th frame + Reduce FPS input by half
     * Better Result

5. [Async + Tracking (Deep-Sort)](https://youtu.be/0NldiTtnT_I?t=1069)
    * Deep Sort uses deep learning to increase tracking accuracy
    * Computation too slow, FPS too low --> bad result

6. [Async + Tracking (Deep-Sort) with improvement 1](https://youtu.be/0NldiTtnT_I?t=1155)
    * Change: Object detection every 6th frame
    * Good Result
7. [Async + Tracking (Deep-Sort) with improvement 2](https://youtu.be/0NldiTtnT_I?t=1251)
     * Change: every 6th frame + Reduce FPS input by half
     * Good Result
### [**Summary of Results**](https://youtu.be/0NldiTtnT_I?t=1319)

## Q&A

1. [How does improvement 1 & 2 improve model FPS](https://youtu.be/0NldiTtnT_I?t=1446)

2. [Can the model detect children or disabled people?](https://youtu.be/0NldiTtnT_I?t=1559)

2. [Other Applications??](https://youtu.be/0NldiTtnT_I?t=1784)

### [**Highlight**](https://youtu.be/0NldiTtnT_I?list=PLrv1bMXfNtLbLvUes3rTG-XNzA7IAuX-_&t=1253)