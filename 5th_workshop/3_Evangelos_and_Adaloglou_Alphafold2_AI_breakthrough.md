# [5th online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05)
## Speakers : Evangelos-Marios & Nikolas Adaloglou

## Topic: [Alphafold2 – the AI breakthrough in a bioinformatics perspective](https://www.nordlinglab.org/workshop-oncvai-05/#SEM)

Takeaways: 

* Alphafold2 is currently state of the art methods for prediction of protein structures with deep learning

Sections of the talk:


## [**What is a protein?**](https://youtu.be/XWXP-pwh4Ek?t=295)
* How Protein is created: DNA --> mRNA --> Protein
* What is a protein? Complex molecules with many functions
    * what are amino acids?  
    * [What is genetic code?](https://youtu.be/XWXP-pwh4Ek?t=393)


## [**Protein structures Levels**](https://youtu.be/XWXP-pwh4Ek?t=441)
* Each levels is a combination of the previous level structure
* Primary: sequence of aminoacids
* Secondary: known repeating geometric pattens (most common: α & β)
* Tertiary: a combination of secondary features makes a tertiary structure
* Quaternary: protein interaction 
* Not all proteins have four levels 

## **Characteristics of protein structure: Domains, Motifs, Turns** 
### [Domains](https://youtu.be/XWXP-pwh4Ek?t=576)
* funtional independent from protein
### [Motifs](https://youtu.be/XWXP-pwh4Ek?t=654)
* connection of different secondary structures
### [Turns & Loops](https://youtu.be/XWXP-pwh4Ek?t=718)
* connectors between α & β structures


## [**MSA (Multiple Sequence alignment)**](https://youtu.be/XWXP-pwh4Ek?t=784)
* Methods to align 3 or more biological sequences
* What are biological sequences? --> Protein, DNA , & RNA
* [What is coevolution?](https://youtu.be/XWXP-pwh4Ek?t=840)


## [**Protein Folding vs Structure Prediction**](https://youtu.be/XWXP-pwh4Ek?t=1038)
* Folding = Process
* Structure = Outcome
* AlphaFold does NOT solve the folding, only predicts structure

## [**Association of biology with ML model design // model workflow**](https://youtu.be/XWXP-pwh4Ek?t=1136)
* Pre-processing // Input 
* Heavy processing (evo transformer)
* Prediction

## [**Representing Amino Acid sequences**](https://youtu.be/XWXP-pwh4Ek?t=1310)
* Continuous representation using embeddings

## **Self-attention**
* [Self attention mechanism](https://youtu.be/XWXP-pwh4Ek?t=1430)
* [Axial attention mechanism (for MSA)](https://youtu.be/XWXP-pwh4Ek?t=1597)
  
## [**MSA transformer concept**](https://youtu.be/XWXP-pwh4Ek?t=1692)

## [**Invariance & Equivariance**](https://youtu.be/XWXP-pwh4Ek?t=1745)
* Position dependent or independent features

## [**AlphaFold 2 core self-attentinon module**](https://youtu.be/XWXP-pwh4Ek?t=1794)
* [Quaternions instead of 3x3 rotation matrix](https://youtu.be/XWXP-pwh4Ek?t=1865)

## **Q&A**

1. [Why is protein structure so important?](https://youtu.be/XWXP-pwh4Ek?t=2044)
1. [Why recycling 3 times](https://youtu.be/XWXP-pwh4Ek?t=2112)

1. [Can the method only capture a single snapshot of the  folding process?](https://youtu.be/XWXP-pwh4Ek?t=2191)

1.  [Are there available database to predict the folding state](https://youtu.be/XWXP-pwh4Ek?t=2324)

1.  [How does the model rank bioinformatic features?](https://youtu.be/XWXP-pwh4Ek?t=2440)

1.  [How to know the aminoacids symbols to do the continuous vector representations](https://youtu.be/XWXP-pwh4Ek?t=2556)

1.  [AlphaFold 2 future improvements](https://youtu.be/XWXP-pwh4Ek?t=2672)

8. [Challenges of colaboration between biologists and engineers?](https://youtu.be/XWXP-pwh4Ek?t=2728)