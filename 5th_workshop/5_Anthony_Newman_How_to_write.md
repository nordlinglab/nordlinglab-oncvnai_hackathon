# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Anthony Newman, Senior Publisher at Elsevier
## Title: [How to write great papers and get them published](https://www.nordlinglab.org/workshop-oncvai-05/#SAN)

Takeaways: 

* When writing a manuscripts, keep in mind what the editor and reviewers are want to see
* 70 percent of manuscripts are rejected. Below some tips to in case of rejection.

Sections of the talk:

## Part 1
### [**Why publish and what to publish?**](https://youtu.be/IFhiHkhTMVQ?t=296)
* New & useful
* No duplicates 


### [**What makes a good manuscript**](https://youtu.be/IFhiHkhTMVQ?t=367)
* Logical not necessarily chronological
* Fail to state importance of your work. Don't assume reader will know

### **Before you write**: 
* [Gather information](https://youtu.be/IFhiHkhTMVQ?t=505)
* [Question before you write](https://youtu.be/IFhiHkhTMVQ?t=533)


### [**What type of manuscript**](https://youtu.be/IFhiHkhTMVQ?t=566)

### [**Think of the reader. How to select right candidate journal?**](https://youtu.be/IFhiHkhTMVQ?t=641)
* Readers
* References 
* Reware of fake journals
* Rarrow down list of candidates and sequence
* Simultaneous publication is unethical, may get banned

### [**Find right audience**](https://youtu.be/IFhiHkhTMVQ?t=767)

### [**Read the Guide to authors**](https://youtu.be/IFhiHkhTMVQ?t=885)
* Modify manuscript accordingly for each journal

### [**Beware of:**](https://youtu.be/IFhiHkhTMVQ?t=943)
* Out of scope
* Wrong format 
* Work with the editors to avoid these 

### [**Scientific Language**](https://youtu.be/IFhiHkhTMVQ?t=1026)
* Use "short snappy sentences"
* Review *guide to authors*

### [**Authorship**](https://youtu.be/IFhiHkhTMVQ?t=1102)
* Author **not equal to** acknowledged individual
* Order of names?
* How to proceed if a contributing member changed institution?

### [**Article Structure**](https://youtu.be/IFhiHkhTMVQ?t=1285)
* Use fish structure
* Make title attractive
* Make your mainn text concise

### [**Order in which to write your article**](https://youtu.be/IFhiHkhTMVQ?t=1404)
* Non-chronological. Use wall approach
    * Figures, Tables
    * Methods, Discussion, Results
    * Conclusion, Introduction
    * Title & Abstract
* After writing is complete rearrange into fish structure

#### [Title](https://youtu.be/IFhiHkhTMVQ?t=1454)
* Hook readers
* Avoid acronyms?

#### [Test keywords](https://youtu.be/IFhiHkhTMVQ?t=1490)
* Check if your keywords return similar papers
* Not to general nor to specifics

#### [Abstract](https://youtu.be/IFhiHkhTMVQ?t=1573)
* Brief 50~250 words
* Speeds up review process

#### [Introduction](https://youtu.be/IFhiHkhTMVQ?t=1660)
* Answer key questions
* Go from general to specific

#### [General tips](https://youtu.be/IFhiHkhTMVQ?t=1708)

#### [Methods](https://youtu.be/IFhiHkhTMVQ?t=1777)
* Detailed enough to be replicated
* Use past tense
* Include experiments and equpitment used
* No discussion
* Mentioned specific chemical used if any
    
#### [Results](https://youtu.be/IFhiHkhTMVQ?t=1895)
* Share mininum possible, only main findings
* Emphasize differeing results to previous findings
* Statistical analysis
* [Figures & Tables](https://youtu.be/IFhiHkhTMVQ?t=1983)
* [Appearance](https://youtu.be/IFhiHkhTMVQ?t=2046)
    
#### [Discussion](https://youtu.be/IFhiHkhTMVQ?t=2143)
* Explaining what your results mean
* Discuss **all** of  your results
* Don't introduce new points, should be done before the dsicussion

#### [Conclusions](https://youtu.be/IFhiHkhTMVQ?t=2244)
* Application
* What has been done?
* What can/will be improved?
* Do Not summarize
* Do not praise your paper

#### [References](https://youtu.be/IFhiHkhTMVQ?t=2303)
* Again follow Guide to Authors
* Check puntuation, names and format

### [**Cover Letter**](https://youtu.be/IFhiHkhTMVQ?t=2372)

### [**Check for Potential reviewers**](https://youtu.be/IFhiHkhTMVQ?t=2421)

## Part 2
### [**Peer Review Process**](https://youtu.be/LoSs8iNi_sg?t=41)
* Need 2 reviewers to accept
* Motives for rejection 
    * Out of scope
    * Poor english
* Reviewers don't accept the paper, they only make recommendations
* [What to do if refected?](https://youtu.be/LoSs8iNi_sg?t=151)
    * Read feedback to learn why it was rejected

### [**Manuscript Revision**](https://youtu.be/LoSs8iNi_sg?t=277)
* Point out changes made in response to reviewers recommendations
* You can write rebuttals, but be polite

### [**Publication Ethics**](https://youtu.be/LoSs8iNi_sg?t=464)

### [**Make the most out of your resources**](https://youtu.be/LoSs8iNi_sg?t=506)
* Researchacademy.elvesier.com
* Check your local library

## Q&A
1. [Hot topcis](https://youtu.be/LoSs8iNi_sg?t=779)
2. [Authorship](https://youtu.be/LoSs8iNi_sg?t=829)
3. [How to find a journal for a multidisciplinary article?](https://youtu.be/LoSs8iNi_sg?t=950)
4. [What happens after journal is submitted to a webpage?](https://youtu.be/LoSs8iNi_sg?t=1069)
5. [Do editors start reviewing immediately after submission, or are there intermediate steps?](https://youtu.be/LoSs8iNi_sg?t=1199)