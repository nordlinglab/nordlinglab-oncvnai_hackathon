# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/) 
## Speakers: Rain Wu, Jacob Chen, Rick Tu, Jose Chang
## Title: [5th Hackathon](https://www.nordlinglab.org/workshop-oncvai-05/#SHT)

Takeaways:

* Powerful pose estimation with OpenPose algorithm provides high accuracy with fast computation

Sections of the talk:

## [What is pose estimation?](https://youtu.be/zXUS3FV3OAc?t=175)
* Depends on key points to represent object/person

## [Pose estimation methods](https://youtu.be/zXUS3FV3OAc?t=262)
* Divided into 2-D and 3-D 
* 3-D methods can be with a single or multiple cameras
* In general most methods consist of the following approaches
    * Top-bottom
    * Bottom-up
## [2-D pose estimation](https://youtu.be/zXUS3FV3OAc?t=561)
  * [Top-down](https://youtu.be/zXUS3FV3OAc?t=607)
    * Detect object from image and detect pose within picture
  * [Bottom-up](https://youtu.be/zXUS3FV3OAc?t=819)
    * Confidence map of where a keypoints could be
  * [When to use which?](https://youtu.be/zXUS3FV3OAc?t=1010)
    * Top-down: fewer people & high precision
    * Bottom-up: lot of people & high FPS


## OpenPose
* [Why use OpenPose Model?](https://youtu.be/zXUS3FV3OAc?t=1099)
    * Computational time doesn't increase much with number of people in image
* [what is OpenPose?](https://youtu.be/zXUS3FV3OAc?t=1191)
    * A bottom-up approach
    * Iterative 
    * The outputs of this model are the confidence map & Parts Affinity Field PAF
    * [What is a Confidence Map](https://youtu.be/zXUS3FV3OAc?t=1410)
    * [What is Part Affinity Field](https://youtu.be/zXUS3FV3OAc?t=1520)
        * It is used for the association of the detected keypoints
* [OpenPose model architecture](https://youtu.be/zXUS3FV3OAc?t=1663)
     * Human model keypoints 

 
## [How to get the code from bitbucket repository](https://youtu.be/zXUS3FV3OAc?t=1893)

## [Human pose detection demonstration](https://youtu.be/zXUS3FV3OAc?t=2016)

## [OpenPose Pipeline Diagram](https://youtu.be/zXUS3FV3OAc?t=2118)
* New NN node
* Single Shot Detector node for pose estimation
* Must create a neural network node instead

## [OpenPose model flowchart](https://youtu.be/zXUS3FV3OAc?t=2285)



## OVERALL CODE EXPLANATION

* [Comments regarding the code](https://youtu.be/zXUS3FV3OAc?t=2425)

### Overall structure

* [Import packages](https://youtu.be/zXUS3FV3OAc?t=2564)

* [Define pipeline](https://youtu.be/zXUS3FV3OAc?t=2703)

* [Link, start pipeline & set queues](https://youtu.be/zXUS3FV3OAc?t=2835)


### [Post processing//OpenPose Algorithm](https://youtu.be/zXUS3FV3OAc?t=2938)
1. Get rgb frame and NN detection 
2. Get confidence map & reshape
     * [Why reshape (1,19,32,57)](https://youtu.be/zXUS3FV3OAc?t=3020) 
3.  [Confidence Map](https://youtu.be/zXUS3FV3OAc?t=3053)
       * Resize & compare to original image
       * Filter out probable keypoints (threshold)
       * Define contours 
       * Get max value of each blob and append to keypoint list
4. [PAF](https://youtu.be/zXUS3FV3OAc?t=3200)
5. [How to choose valid pair of candidate keypoints (KP's)](https://youtu.be/zXUS3FV3OAc?t=3237)
    * Normalize candidate KP's
* Interpolate location of KP's
* Find paf value at each interpolated location
* If kp meet criteria (pafs score & avg sim) append to valid pair list 
6. Find correct connection of valid KP pairs
7. Save 
8. Plot



## CODE: PART BY PART
### Setup
* [Import packages](https://youtu.be/zXUS3FV3OAc?t=3516)
* *Pose_info.decode* & *Pose_info.plotting*: OpenPose model specific parameters
* [Define pipeline](https://youtu.be/zXUS3FV3OAc?t=3692)
    * Create nodes: color camera node & nn node
* [Link nodes,start pipeline & define queues](https://youtu.be/zXUS3FV3OAc?t=3981)

### Post-Processing: Pose estimation Algorithm 
* [Get items from queue: RGB cam & nn node](https://youtu.be/zXUS3FV3OAc?t=4215)
    * For nn detection use `.tryget()`
    * Convert RGB frame to cv.frame
* [Check for detections](https://youtu.be/zXUS3FV3OAc?t=4367)
    * Define confidence map
    * Define paf
    * [Filter image for possible candidate keypoints](https://youtu.be/zXUS3FV3OAc?t=4710)
        * Demonstration for keypoint_0=head & keypoint_3=elbow 
    * [Create a mask probability map: remove values below threshold](https://youtu.be/zXUS3FV3OAc?t=5070)
        * Used to remove values below threshold
    * [PAF map](https://youtu.be/zXUS3FV3OAc?t=5320)

   
