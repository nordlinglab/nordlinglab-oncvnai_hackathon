# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Rama A. Salhi, Clinical Lecturer at University of Michigan
## Title: [How to tackle order of writing a scientific article](https://www.nordlinglab.org/workshop-oncvai-05/#SRS)

Takeaways: 

* Tables & figures should be complementary not redundant

Sections of the talk:

### [**Puzzle analogy**](https://youtu.be/tpXa41rstrA?t=330)


### [**Structure of a Manuscript**](https://youtu.be/tpXa41rstrA?t=353)
* Abstact: summary of work
* Introduction: base knowledge
* Methods: detailed enough for reproduction
* Results: provide info to interpret results
* Discussion: main results and application in context of field of study

### [**How to read a Manuscript**](https://youtu.be/tpXa41rstrA?t=421)
Proposed reading order:
#### *Especially useful for readers who are not experts in the topic*
1. Abstract: to know if article contains the info I'm looking for 
2. Introduction : to know what base knowledge is needed to understand the rest of the paper
3. Discussion : Know main outcome in the context of the related field of study
#### *Optional: only if more in depth info is needed*
4. Results
5. Methods
#### [When is it critical to read the methods?](https://youtu.be/tpXa41rstrA?t=558)
#### [Why approach papers in this order?](https://youtu.be/tpXa41rstrA?t=587)

   
###  **How to write a manuscript (order opposite of reading a manuscript)**
#### [Template used for examples](https://youtu.be/tpXa41rstrA?t=615)
#### [1. How to write Methods? What sould be included?](https://youtu.be/tpXa41rstrA?t=640)
* In order of appearing results
* Should include:
    * Data Collection
    * Study Population
    * Analytic Methods
* [Example](https://youtu.be/tpXa41rstrA?t=722)

#### [2. Results](https://youtu.be/tpXa41rstrA?t=761)
* Same order as methods
* [To avoid](https://youtu.be/tpXa41rstrA?t=776)
    * If it wasn't in methods should no be in results
    * No interpretation of results
    * Tables & figures should be complementary not redundant
        * [Example](https://youtu.be/tpXa41rstrA?t=849)
* [Rules to tables & figures](https://youtu.be/tpXa41rstrA?t=903)
    * Don't cluttler
    * Clear & legible
    * [Example](https://youtu.be/tpXa41rstrA?t=989)

#### [3. Discussion/Conclusion](https://youtu.be/tpXa41rstrA?t=1060)
* [Dont's](https://youtu.be/tpXa41rstrA?t=1098)
    * Unsupported conclusions
    * New terms not mentioned before
* Do's
    * Clear & concise
* [Example](https://youtu.be/tpXa41rstrA?t=1143)
  
#### [4. Introduction](https://youtu.be/tpXa41rstrA?t=1197)
* Teach foundational knowledge
* [Example](https://youtu.be/tpXa41rstrA?t=1244)

#### [5. Abstract](https://youtu.be/tpXa41rstrA?t=1293)

### [**Why is it important?**](https://youtu.be/tpXa41rstrA?t=1344)

## Q&A

1. [When should I start writing? Should include all results or only useful ones?](https://youtu.be/tpXa41rstrA?t=1509)

1. [Which section takes the most time? Which one is the most important?](https://youtu.be/tpXa41rstrA?t=1597)

1. [How to overcome writer block?](https://youtu.be/tpXa41rstrA?t=1665)

1. [What is the most common reason for rejection?](https://youtu.be/tpXa41rstrA?t=1736)

1. [Should target specific journals for writing?](https://youtu.be/tpXa41rstrA?t=1819)

1. [What is the approach to target journals?](https://youtu.be/tpXa41rstrA?t=1890)
