# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Pierre-Nicolas Tiffreau, [Founder & CTO of Picsellia](https://youtu.be/yZTWz91hoeg?t=275)
## Title: [Road to Machine Learning in Production](https://www.nordlinglab.org/workshop-oncvai-05/#SPT)
Recommended reading: [Scaling AI like a tech native: The CEO's role](https://www.mckinsey.com/business-functions/mckinsey-analytics/our-insights/scaling-ai-like-a-tech-native-the-ceos-role)

Takeaways: 

* 70~90% projects don't go beyond prototype stage, but Mlops can help address this problem

Sections of the talk:
## [Abstract: Taking a ML/DL project from protopype phase to business application](https://youtu.be/yZTWz91hoeg?t=160)


## **Concepts:** 

### [AI lifecyle](https://youtu.be/yZTWz91hoeg?t=314)
* General process:
    1. Data acquisition & management
    2. Model training and optimization
    3. Deployment 
### Data-centric AI 
* [What is model-centric approach? (As opposed to data-centric)](https://youtu.be/yZTWz91hoeg?t=358)
    * More focus on the model, less focus on data
* Disadvantage: many manual processes  --> complicated to apply
    * Data acquisition & management
    * Model training etc..
* [Data Centric](https://youtu.be/yZTWz91hoeg?t=479)
    * Fixed model, changing data
    * Quality over quantity of data
    * Consistent labeling

### [MLOps: Machine Learning Operations](https://youtu.be/yZTWz91hoeg?t=591)
* Automation of core ML processes:
    * Training
    * Validation
    * Deployment
* [MLOps levels](https://youtu.be/yZTWz91hoeg?t=631)
    * level 0
    * level 1
    * level 2 
* [What is not MLops?](https://youtu.be/yZTWz91hoeg?t=666)
    * Notebook approach
    * Monitoring

#### [MLOps lvl 0 (Manual procesess)](https://youtu.be/yZTWz91hoeg?t=766) 
* Unefficient iterative process
* Low adaptability
* Flowchart(https://youtu.be/yZTWz91hoeg?t=878)

####  [MLOps lvl 1 (continuous training)](https://youtu.be/yZTWz91hoeg?t=898)
* New data can be tested automatically
* Single pipeline/ mantaining multiple pipeline demanding

#### [MLops lvl 2 (fully automated continuous integration)](https://youtu.be/yZTWz91hoeg?t=1013)
* Fully automated continuous integration
* Remaining Manual Steps: data & error analysis

## [**The MLOps Tools/Techniques**](https://youtu.be/yZTWz91hoeg?t=1084)
### [Deployment](https://youtu.be/yZTWz91hoeg?t=1137)
  * Servers
    * [Cons of Flask Deployment](https://youtu.be/yZTWz91hoeg?t=1225)
  * Edge Devices
    * [Pros & Cons: Tensorflow serving](https://youtu.be/yZTWz91hoeg?t=1286)
    * [Tensorflow serving examples](https://youtu.be/yZTWz91hoeg?t=1367)
  * [Other options](https://youtu.be/yZTWz91hoeg?t=1403)
    * Graph Pipe
    * MLFLow
    * SimpleTensorFlow Serving
    * Cloud Services (AWS, Google, ...)
### Monitoring
  * [Dangers](https://youtu.be/yZTWz91hoeg?t=1531)
  * [What should be monitored](https://youtu.be/yZTWz91hoeg?t=1602)
  * [@ lvl 0](https://youtu.be/yZTWz91hoeg?t=1713)
  * [@ lvl 1](https://youtu.be/yZTWz91hoeg?t=1726)
  * [@ lvl 2](https://youtu.be/yZTWz91hoeg?t=1790)
  * [Tools](https://youtu.be/yZTWz91hoeg?t=1812)
* [FeedBack Loop](https://youtu.be/yZTWz91hoeg?t=1875)
* [Continuous Training](https://youtu.be/yZTWz91hoeg?t=1938)
* [Continuous Delivery](https://youtu.be/yZTWz91hoeg?t=1980)
  
## **Demontration**
### [Architecture](https://youtu.be/yZTWz91hoeg?t=2066)

### [Picsellia Platform](https://youtu.be/yZTWz91hoeg?t=2103)
* Train models

### [Dataset for FaceDetection](https://youtu.be/yZTWz91hoeg?t=2124)
* 500 images
* Inconsistent labels

### [Face Detection Models](https://youtu.be/yZTWz91hoeg?t=2160)
* Several iterations of the model
* Model experimentation
* Tunable hyperparameters
* Export models

### [Pipeline: Falcon Web Server](https://youtu.be/yZTWz91hoeg?t=2273)
* Inference task
* [Webhooks](https://youtu.be/yZTWz91hoeg?t=2326)

### [Data input](https://youtu.be/yZTWz91hoeg?t=2380)
* Webcam --> server --> Picsellia Serving

### [Adding data to model in real time](https://youtu.be/yZTWz91hoeg?t=2460)
* Resizing anotation 
* Retraining model iterations
* [Results after re-training & re-deployment](https://youtu.be/yZTWz91hoeg?t=2764)


## Q&A
1. [What is the place of MLops for academia?](https://youtu.be/yZTWz91hoeg?t=2930)

1. [Monetary cost to go from MLops lvl 0 --> lvl 1](https://youtu.be/yZTWz91hoeg?t=2987)

1. [How to monitor training process?](https://youtu.be/yZTWz91hoeg?t=3050)

1. [What is the criteria to evaluate the training?](https://youtu.be/yZTWz91hoeg?t=3124)