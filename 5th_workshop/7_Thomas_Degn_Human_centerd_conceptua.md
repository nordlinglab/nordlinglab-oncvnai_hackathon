# [5h Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Thomas Degn, Associate Professor in Industrial Design at Umeå University
## Title: [Human-centred conceptual product design as an exploration tool to imagine and enact the future of medical devices](https://www.nordlinglab.org/workshop-oncvai-05/#RTD)

Takeaways: 

* A patients mental state and mood is just as important as his physical vital signs

Sections of the talk:

### [**Umeå University collaboration in the medical field**](https://youtu.be/U0nM2whoNeY?t=169)

### [**Future Intensive Care Monitoring Project**](https://youtu.be/U0nM2whoNeY?t=387)
* [Research trip to Getinge, Stockholm](https://youtu.be/U0nM2whoNeY?t=518)
* [Clinical training center](https://youtu.be/U0nM2whoNeY?t=700)

### **Three projects:**
1. Array:The next generation ECG 
    * [ECG and its possible to be improvements](https://youtu.be/U0nM2whoNeY?t=784)
    * [Inital concept: (dock - sensor - reciever)](https://youtu.be/U0nM2whoNeY?t=881)
    * [Array protopype testing](https://youtu.be/U0nM2whoNeY?t=925)
    * [Comparison: ECG vs Array](https://youtu.be/U0nM2whoNeY?t=986)
    * [Array video snippet by students](https://youtu.be/U0nM2whoNeY?t=1033)
1. SOVA: Soft Vital Signs Monitoring
    * [Problem statement:  patients in ICU mental health and causes of stress](https://youtu.be/U0nM2whoNeY?t=1275)
    * [What are "soft vital signs"?](https://youtu.be/U0nM2whoNeY?t=1474)
    * [Sova video snippet](https://youtu.be/U0nM2whoNeY?t=1515)
    * [Concept](https://youtu.be/U0nM2whoNeY?t=1659)
        
3. CANNE - Self-directed CPR learning 
    * [Stadistics about cardiar arrest](https://youtu.be/U0nM2whoNeY?t=1819)
    * [Visuazilation of CANNE with UI](https://youtu.be/U0nM2whoNeY?t=2088)
    * [CANNE video Snippet](https://youtu.be/U0nM2whoNeY?t=2389)