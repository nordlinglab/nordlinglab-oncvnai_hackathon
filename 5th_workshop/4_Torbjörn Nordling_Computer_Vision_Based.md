# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Torbjörn Nordling, Assistant Professor at the National Cheng Kung University in Taiwan and Senior Lecturer at Umeå University
## Title: [Computer Vision Based Assessment of Motor Skills—a Step Towards Digitalisation of the Unified Parkinson’s Disease Rating Scale](https://www.nordlinglab.org/workshop-oncvai-05/#STN)

Takeaways: 

* UPDRS is coarse and subjective
* The purpose of improving the severity assesment is to provide better quality of life to the patients

Sections of the talk:

### [**Background & Vision**](https://youtu.be/MzGJ9iOb60w?t=143)
* [What is Parkinson's and who does it affect?](https://youtu.be/MzGJ9iOb60w?t=212)
* [PD severity assesment: current state and deficiencies](https://youtu.be/MzGJ9iOb60w?t=248)
* [Vision](https://youtu.be/MzGJ9iOb60w?t=318)



### [**Litertaure Review**](https://youtu.be/MzGJ9iOb60w?t=379)
* Previous work and defficiencies
* Pairwise differences between raters against mean for total UPDRS-ME score -Post et al. [2005]
    * Subjectivity of UPDRS scale
* Self-managed systme -Ferraris et al. [2018]
    * Glove colored finger tips for CV identification methods
* Lower limbs and postural tasks -Ferraris et al. [2019]
    * Leg agility tests and posture examination
* Upper and lower limbs subsystem -Albani et al. [2019]


### [**Experiment Setup**](https://youtu.be/MzGJ9iOb60w?t=682)
* Aim: portable, reproducible, consistent measurements, reduce data variation 
* [First iteration](https://youtu.be/MzGJ9iOb60w?t=767)
    * Chessboards for position reference calibration
    * Multiple cameras at different positions
    * Table for video synchronisation
* Second iteration
    * Change position of front camera 
* [Third iteration (current)](https://youtu.be/MzGJ9iOb60w?t=890)
    * White markers for hand/foot placement reference
    * Oak-D cameras
    * Simplified design for foot box
    * Standard hospital chair considered for box design
* [Camera views](https://youtu.be/MzGJ9iOb60w?t=1050)
    * Hand box: back view

* [Sequence 7 diff motor tests](https://youtu.be/MzGJ9iOb60w?t=1131)
    * Hand tests
        * Sync motion
        * Finger tapping    
        * Rest tremor
        * Hand movement
        * Postural tremor
    * Foot tests
        * Leg agility test
        * Toe tapping 
        * Rest tremor

* Demonstration videos for subjects
    * [Hand demonstration video](https://youtu.be/MzGJ9iOb60w?t=1375)
    * [Foot demonstration video](https://youtu.be/MzGJ9iOb60w?t=1437)


### [**Data Collection**](https://youtu.be/MzGJ9iOb60w?t=1529)
* Subjects:
    * 20 healthy (control), 
    * 18 PD patients
    * 1 Parkisonism: has mild symptopms, but not severe enough to be diagnosed with PD
* Some of foot tests (incomplete recording)

### [**Preliminary results**](https://youtu.be/MzGJ9iOb60w?t=1689)
 * Methods
    * Stickers (color filter)
    * Euclidean distance calculation
 * Explanation of graphs 
    * Sticker distance measurement
    * Open/close movement transition
    * Noise & uncertainty of measurements
 * [Finger tapping test results](https://youtu.be/MzGJ9iOb60w?t=1840)
 * [Toe tapping test results](https://youtu.be/MzGJ9iOb60w?t=1998)
 * [Hand movement test results](https://youtu.be/MzGJ9iOb60w?t=2134)

### [**Challenges**](https://youtu.be/MzGJ9iOb60w?t=2277)
* Limitation of 2d tracking: unclear open to close transition
* Count of pixel skin inssuficient--> track hand features instead
* Real scenario cameras will most likely have lower quality

### [**Road map forward**](https://youtu.be/MzGJ9iOb60w?t=2583)
* Deep Feature Encodings to recognize skin features
* Implementation of pose estimation for hands 
* Capture depth using stereo vision
* Time domain: single frame analysis --> analize sequence of frames
* Data collection: large number of samples needed

