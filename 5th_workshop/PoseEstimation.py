# Import required packages

import cv2
import depthai as dai
import numpy as np
from Pose_info.plotting import pose_pairs, mapIdx, colors, parts_num, pairs_num, keypoints_num
from Pose_info.decode import model_path, frame_size, kp_conf_layer, kp_conf_shape, paf_layer, paf_shape, n_interp_samples, paf_score_th, conf_th

# Defind pipeline 
def createPoseEstimationPipeline(blob_path, prev_size):
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setPreviewSize(prev_size)
    cam_rgb.setInterleaved(False)
    xout_cam_rgb = pipeline.createXLinkOut()
    xout_cam_rgb.setStreamName('rgb')
    print('Created color camera object')

    pose_nn = pipeline.createNeuralNetwork()
    pose_nn.setBlobPath(blob_path)
    xout_pose_nn = pipeline.createXLinkOut()
    xout_pose_nn.setStreamName('pose_nn')
    print('Created pose detection NN')

    # Linking
    cam_rgb.preview.link(pose_nn.input)
    cam_rgb.preview.link(xout_cam_rgb.input)
    pose_nn.out.link(xout_pose_nn.input)
    print('Finished creating pipeline')
    return pipeline


# start pipeline
pipeline = createPoseEstimationPipeline(model_path, frame_size)
device = dai.Device(pipeline)
device.startPipeline()
q_rgb = device.getOutputQueue("rgb", maxSize = 1, blocking = False)
pose_nn = device.getOutputQueue("pose_nn", maxSize = 1, blocking = False)

# Initialize variables
pose, personwiseKeypoints = None, None
threshold, nPoints = 0.2, 18
detected_keypoints = []

while(True):
    # Get items from queue
    in_rgb = q_rgb.get()
    in_pose_detections = pose_nn.tryGet()
    rgb_frame = in_rgb.getCvFrame()
    if in_pose_detections is not None: 
        # get keypoints confidence map and Part Affinity Fields (PAFs) map from prediction
        conf_map = np.array(in_pose_detections.getLayerFp16(kp_conf_layer)).reshape(kp_conf_shape)
        pafs = np.array(in_pose_detections.getLayerFp16(paf_layer)).reshape(paf_shape)
        new_keypoints = []
        new_keypoints_list = np.zeros((0, 3))
        keypoint_id = 0
        for i in range(nPoints):
            probMap = conf_map[0, i, :, :] #(32,57)
            # Resize the keypoints confidence map
            probMap = cv2.resize(probMap, frame_size)
            # Binarize the image
            mapMask = np.uint8(probMap > threshold)
            # Find contours
            contours, _ = cv2.findContours(mapMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            keypoints = []
            for cnt in contours:
                # Identify pixel liekly a keypint
                blobMask = np.zeros(mapMask.shape)
                blobMask = cv2.fillConvexPoly(blobMask, cnt, 1)
                maskedProbMap = probMap * blobMask
                # Find the maximum confidence location for each keypoint
                _, maxVal, _, maxLoc = cv2.minMaxLoc(maskedProbMap)
                keypoints.append(maxLoc + (probMap[maxLoc[1], maxLoc[0]],))
            new_keypoints_list = np.vstack([new_keypoints_list, *keypoints])
            keypoints_with_id = []
            for j in range(len(keypoints)):
                keypoint_with_id = (keypoints[j][0],keypoints[j][1],keypoints[j][2],keypoint_id)
                keypoints_with_id.append(keypoint_with_id)
                keypoint_id += 1
            new_keypoints.append(keypoints_with_id)
        valid_pairs = []
        n_interp_samples, paf_score_th, conf_th = 10, 0.2, 0.4
        for i in range(len(mapIdx)):
            # Resize the PAFs map
            pafX = pafs[0, mapIdx[i][0]-pairs_num, :, :] # shape =  (32,57)  # x
            pafY = pafs[0, mapIdx[i][1]-pairs_num, :, :] # shape =  (32,57)  # y
            pafX = cv2.resize(pafX, frame_size)
            pafY = cv2.resize(pafY, frame_size)
            # Define candidate keypoints
            candA = new_keypoints[pose_pairs[i][0]]
            candB = new_keypoints[pose_pairs[i][1]]
            valid_pair = np.zeros((0, 3))
            for j in range(len(candA)):
                max_j, maxScore, found = -1, -1, 0
                for k in range(len(candB)):
                    # Normalize the residual vector of candidate keypoints pairs to unit length.
                    norm = np.linalg.norm(np.subtract(candB[k][:2], candA[j][:2]))
                    d_jk = np.subtract(candB[k][:2], candA[j][:2])
                    d_jk = d_jk/norm
                    # Interpolate between the location of the candidate keypoints
                    interp_coord = list(zip(np.linspace(candA[j][0], candB[k][0], num=n_interp_samples), np.linspace(candA[j][1], candB[k][1], num=n_interp_samples)))
                    paf_interp = []
                    for q in range(len(interp_coord)):
                    # Find the PAF values at each interpolated location
                        x,y = int(interp_coord[q][1]), int(interp_coord[q][0])
                        paf_interp.append([pafX[x,y], pafY[x,y]])
                    paf_pojection_score = (len(np.where(np.dot(paf_interp, d_jk) > paf_score_th)[0]) / n_interp_samples)

                    average_similarity = sum(np.dot(paf_interp, d_jk)) / len(np.dot(paf_interp, d_jk))

                    if paf_pojection_score > conf_th and average_similarity > maxScore:
                    # Save pairs if projection score and similarity higher than threshold 
                        found = 1
                        maxScore = sum(np.dot(paf_interp, d_jk)) / len(np.dot(paf_interp, d_jk))
                        valid_pair = np.append(valid_pair, [[candA[j][3], candB[k][3], maxScore]], axis=0)
            valid_pairs.append(valid_pair)

        keypoints_list = new_keypoints_list
        personwiseKeypoints = -1 * np.ones((0, pairs_num))
        for i in range(len(mapIdx)):  # 19
        # Find correct connection in valid_pairs
            if len(valid_pairs[i]) != 0:
                partAs, partBs = valid_pairs[i][:, 0], valid_pairs[i][:, 1]
            indexA, indexB = np.array(pose_pairs[i])
            for j in range(len(valid_pairs[i])):
                found = 0
                for k in range(len(personwiseKeypoints)):
                # Save connection if it's in the valid_pairs
                    if personwiseKeypoints[k][indexA] == partAs[j]:
                        found = 1
                        personwiseKeypoints[k][indexB] = partBs[j]
                        personwiseKeypoints[k][-1] += keypoints_list[partBs[j].astype(int), 2] + valid_pairs[i][j][2]
                if not found and i < parts_num:
                # Save connection if it's not in list
                    row = -1 * np.ones(pairs_num)
                    row[indexA] = partAs[j]
                    row[indexB] = partBs[j]
                    row[-1] = sum(keypoints_list[valid_pairs[i][j, :2].astype(int), 2]) + valid_pairs[i][j][2]
                    personwiseKeypoints = np.vstack([personwiseKeypoints, row])
        detected_keypoints, keypoints_list, personwiseKeypoints = (new_keypoints, new_keypoints_list, personwiseKeypoints)
        for i in range(keypoints_num):
            for j in range(len(detected_keypoints[i])):
                cv2.circle(rgb_frame, detected_keypoints[i][j][0:2], 5, colors[i], -1, cv2.LINE_AA)
        for i in range(parts_num):
            for j in range(len(personwiseKeypoints)):
                index = personwiseKeypoints[j][np.array(pose_pairs[i])]
                if -1 in index: continue
                B = np.int32(keypoints_list[index.astype(int), 0])
                A = np.int32(keypoints_list[index.astype(int), 1])
                cv2.line(rgb_frame, (B[0], A[0]), (B[1], A[1]), colors[i], 3, cv2.LINE_AA)
        cv2.imshow("rgb", rgb_frame)
    if cv2.waitKey(1) == ord('q'): break
