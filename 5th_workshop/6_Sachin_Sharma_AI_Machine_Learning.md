# [5th Online Computer Vision & AI Workshop](https://www.nordlinglab.org/workshop-oncvai-05/)
## Speaker: Sachin Sharma, Co-Founder of Funoppia
## Title: [AI & Machine Learning for young minds – using Funoppia’s FunAI IDE](https://www.nordlinglab.org/workshop-oncvai-05/#SSS)

Takeaways: 

* Funoppia is a friendly resource for anyone wanting to learn AI

Sections of the talk:

### [**What is FUNOPPIA?**](https://youtu.be/u-7e-9-VgHo?t=226)
* A free visual platform for learning AI and CV
* [Linear regression demonstration](https://youtu.be/u-7e-9-VgHo?t=331)

### [**Computer Vision**](https://youtu.be/u-7e-9-VgHo?t=496)
* What is computer vision?
* Many, many applications
* Relies on deep learning algorithms specifically: CNN


### [**Convolutional Neural Network CNN**](https://youtu.be/u-7e-9-VgHo?t=593)
* Workflow of how a CNN works

#### [Images](https://youtu.be/u-7e-9-VgHo?t=664)
* What is a pixel?
* What do channels in an image represent?
* [Pixel value demontration](https://youtu.be/u-7e-9-VgHo?t=757)

#### [The convolution operation](https://youtu.be/u-7e-9-VgHo?t=834)
* Filters as matrices
* Convolution operation
* [Outputs of different types of filters](https://youtu.be/u-7e-9-VgHo?t=945)
* Values of the filters are adjusted automatically by the NN based on the Linear approximation theorem

#### [CNN network layers](https://youtu.be/u-7e-9-VgHo?t=1107)
* [Pooling](https://youtu.be/u-7e-9-VgHo?t=1144)
     * max pooling
     * average pooling
* [How are the weights and biases updated?](https://youtu.be/u-7e-9-VgHo?t=1244)

### [**Varieties of neural networks**](https://youtu.be/u-7e-9-VgHo?t=1331)
* different architectures have different applications

### **Recreating CNN model using FUNNOPIA**
1. [Initialize model & assign variable](https://youtu.be/u-7e-9-VgHo?t=1405)
2. [Setup CNN Network Layers](https://youtu.be/u-7e-9-VgHo?t=1445)
3. [Printing model structure summary](https://youtu.be/u-7e-9-VgHo?t=1702)
4. [Running in google Colab](https://youtu.be/u-7e-9-VgHo?t=1745)
5. [Review of model architecture](https://youtu.be/u-7e-9-VgHo?t=1811)

### [**What if a have a small dataset?**](https://youtu.be/u-7e-9-VgHo?t=1991)
* Data augmentation: crop, rotate, etc ..
* [Transfer learning](https://youtu.be/u-7e-9-VgHo?t=2045)
* [VGG architercture which can be used for transfer learning](https://youtu.be/u-7e-9-VgHo?t=2156)

### [**Project: mask detection**](https://youtu.be/u-7e-9-VgHo?t=2188)
* Detects whether a person is wearing a mask or not
* [Baking cake analogy](https://youtu.be/u-7e-9-VgHo?t=2241)
* [Steps of creating an NN](https://youtu.be/u-7e-9-VgHo?t=2308)
    1. Loading libraries 
    2. Dataset loading & preprocessing 
    3. Creating Network Architecture 
    4. Training Model 
    5. Visualize Training 
    6. Testing & Uploading model in project 
* [Funoppia code-block representation of the steps in the mask detection project](https://youtu.be/u-7e-9-VgHo?t=2379)
* [Running Project in Colabs](https://youtu.be/u-7e-9-VgHo?t=1745)
* [Review of Project steps, Visualization & Results](https://youtu.be/u-7e-9-VgHo?t=2667)

### [**Brief mention of NLP type projects**](https://youtu.be/u-7e-9-VgHo?t=2906)

## Q&A

1. [Does Funoppia have any limitations? Any future features to implement?](https://youtu.be/u-7e-9-VgHo?t=2997)
2. [What is your favorite part of Funoppia?](https://youtu.be/u-7e-9-VgHo?t=3035)
3. [Can Funoppia make code block from code?](https://youtu.be/u-7e-9-VgHo?t=3119)
4. [How many layers shoud we have in our network?](https://youtu.be/u-7e-9-VgHo?t=3157)
5. [Are there any requirements or limitation on users computer to use funoppia?](https://youtu.be/u-7e-9-VgHo?t=3224)